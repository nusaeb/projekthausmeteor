package projekthausmeteor.activity;

import projekthausmeteor.data.dao.DirectionVideoDataSource;
import projekthausmeteor.data.model.DirectionVideo;

import com.example.projekthausmeteor.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class DirectionVideoActivity extends Activity {

	private Context context;
	private VideoView videoViewDirectionVideo;
//	private TextView textViewVideoTitle;
	private ProgressDialog progressDialog;
	private DirectionVideoDataSource directionVideoDataSource;
	private int fromAreaId, toAreaId, originalOrientation;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.direction_video_layout);
		originalOrientation = getResources().getConfiguration().orientation;
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		fromAreaId = getIntent().getIntExtra("fromAreaId", 1);
		toAreaId = getIntent().getIntExtra("toAreaId", 2);
		directionVideoDataSource = new DirectionVideoDataSource(this);
		directionVideoDataSource.open();
		DirectionVideo directionVideo = directionVideoDataSource.getDirectionVideoFrom2To(fromAreaId, toAreaId);
		directionVideoDataSource.close();
		//String videoPath = directionVideo.getVideoPath();
		
		// create a progress bar while the video file is loading
        progressDialog = new ProgressDialog(DirectionVideoActivity.this);
        // set a title for the progress bar
        progressDialog.setTitle("JavaCodeGeeks Android Video View Example");
        // set a message for the progress bar
        progressDialog.setMessage("Loading...");
        //set the progress bar not cancelable on users' touch
        progressDialog.setCancelable(false);
        // show the progress bar
//        progressDialog.show();

        String videoTitle = "Direction from point " + fromAreaId + " to " + toAreaId;
		String videoPath = "android.resource://" + getPackageName() 
				+ "/" + directionVideo.getVideoPath();
		
//		textViewVideoTitle = (TextView)findViewById(R.id.textView_video_title);
//		textViewVideoTitle.setText(videoTitle);
		videoViewDirectionVideo = (VideoView)findViewById(R.id.videoView_direction_video);
		videoViewDirectionVideo.setVideoPath(videoPath);
//		videoViewDirectionVideo.setScrollBarStyle(VideoView.SCROLLBARS_OUTSIDE_OVERLAY);
		
		final MediaController mediaController = new MediaController(this);
		mediaController.setAnchorView(videoViewDirectionVideo);
		videoViewDirectionVideo.setMediaController(mediaController);

		videoViewDirectionVideo.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "clicked..", Toast.LENGTH_SHORT).show();
				
			}
			
		});
		videoViewDirectionVideo.setOnPreparedListener(new OnPreparedListener() {
            // Close the progress dialog and play the video
            public void onPrepared(MediaPlayer mp) {
                
            	//VideoView vide = (VideoView)popupLayout.findViewById(R.id.videoView_popup);
        		//int index = popupLayout.indexOfChild(vide);
        		//popupLayout.removeView(vide);
    			
    			//videoView.setVisibility(View.VISIBLE);
    			//VideoView video = videoView;
    			//popupLayout.addView(videoView, index);
        		//vide = videoView;
        		//vide.start();
            	
//            	progressDialog.dismiss();
            	mediaController.setAnchorView(videoViewDirectionVideo);
            	//setContentView(videoViewDirectionVideo);
            	videoViewDirectionVideo.start();
//            	textViewVideoTitle.bringToFront();
                //videoView.pause();
                
               
                /*mediaController.show();
                mediaController.bringToFront();*/
                
            }
        });
		videoViewDirectionVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				AlertDialog.Builder dialog = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
				dialog.setPositiveButton(R.string.dialog_video_replay, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						videoViewDirectionVideo.start();
						
					}
					
				});
				dialog.setNegativeButton(R.string.dialog_video_finish, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						PageActivity.directionShowed = true;
//						PageActivity.currentPageNum = toAreaId;
						finish();
						setRequestedOrientation(originalOrientation);
						Intent pageIntent = new Intent(context, PageActivity.class);
			    		pageIntent.putExtra("areaId", toAreaId);
			    		context.startActivity(pageIntent);
						
					}
				});
				dialog.show();
				
			}
			
		});
	}
}
