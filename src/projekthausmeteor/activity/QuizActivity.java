package projekthausmeteor.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projekthausmeteor.constant.QuestionType;
import projekthausmeteor.data.dao.QuestionDataSource;
import projekthausmeteor.data.model.Question;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projekthausmeteor.R;

public class QuizActivity extends Activity{

	private static List<Question> allQuestions;
	private static String questionTitle, questionText, currentLinkText;
	private static String answer = "";
	private int currentQuestionNo = 1;
	private int countCorrectAnswer = 0;
	private boolean isEvaluationShowing = false, isAnswerShowing = false;
	private String currentTextInEditText = "";
	private Map<Integer, String> userAnswer;
	private RelativeLayout activityQuizLayout, layoutQuestion, layoutEvaluation;
	private TextView textviewPageTitle, textviewQuestion, link, textviewEvaluationTitle, textviewEvaluationText, textviewEvaluationComment;
	private ImageView imageviewEmoticon;
	private EditText answerEditText;
	private ScrollView scrollViewRadioGroup;
	private RadioGroup answerRadioGroup, savedRadioGroup;
	
	private static Question currentQuestion;
	private QuestionType currentQuestionType;
	private QuestionDataSource dataSource;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
			activityQuizLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.quiz_layout_land, 
					new RelativeLayout(this), false);
        }
        else{
        	activityQuizLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.quiz_layout_port, 
    				new RelativeLayout(this), false);
        }
		// initializing all the views
		initializeViews();
		// setting on focus listener for the edittext view
		setOnFocusListener(answerEditText);
		userAnswer = new HashMap<Integer, String>();
		dataSource = new QuestionDataSource(this);
		dataSource.open();
		// getting all the questions
		QuizActivity.allQuestions = dataSource.getAllQuestionsByAreaId(PageActivity.currentAreaId);
		// setting the 1st question of the list as current question
		QuizActivity.currentQuestion = QuizActivity.allQuestions.get(currentQuestionNo - 1);
		questionTitle = getString(R.string.title_question);
		textviewPageTitle.setText(R.string.title_quiz_page);
		// setting contents of the page
		setPageContents(QuizActivity.currentQuestion);
		setContentView(activityQuizLayout);
		
		// getting the current text of the link at the bottom and storing it in a static attribute
		QuizActivity.currentLinkText = link.getText().toString();
	}
	
	/**
	 * To trace the Onclick event for the link at the bottom of the page.
	 * @param view
	 */
	public void linkClicked(View view){
		// it will show the correct answer of the current question
		if(QuizActivity.currentLinkText.equalsIgnoreCase(getString(R.string.link_text_answer))){
			String userAnswer = getUserAnswer();
			if(!userAnswer.equals("")){
				showAnswer(userAnswer);
			}			
		}
		// it will show the next question
		else if(QuizActivity.currentLinkText.equalsIgnoreCase(getString(R.string.link_text_next_question))){
			if(!getUserAnswer().equals("")){
				showNextQuestion();
			}			
		}
		// it will show the final evaluation at the end of the quiz/survey
		else if(QuizActivity.currentLinkText.equalsIgnoreCase(getString(R.string.link_text_show_evaluation))){
			if(!getUserAnswer().equals("")){
				link.setText(getString(R.string.link_text_back_to_menu));
				QuizActivity.currentLinkText = link.getText().toString();
				showFinalEvaluation();
			}
		}
		// it will take the user back to the menu screen
		else{
			goBackToMenu();
		}
	}
	
	/**
	 * To set the contents of the quiz/survey page.
	 * @param question
	 */
	public void setPageContents(Question question){
		scrollViewRadioGroup = (ScrollView)activityQuizLayout.findViewById(R.id.scrollView_radiogroup);
		answerRadioGroup = (RadioGroup)activityQuizLayout.findViewById(R.id.radioGroup_answer);
		answerEditText = (EditText)activityQuizLayout.findViewById(R.id.editText_answer);
		setOnFocusListener(answerEditText);
		
		// setting question text
		QuizActivity.questionText = questionTitle + (currentQuestionNo) + ":\n\n" + question.getQuestion();
		textviewQuestion.setText(questionText);
		// setting options for multiple choice questions
		if(question.getQuestionType().equals(QuestionType.MULTIPLE_ANSWER.toString())){
			currentQuestionType = QuestionType.MULTIPLE_ANSWER;
			createCheckBoxGroup(question);
		}
		// setting options for single choice questions
		if(question.getQuestionType().equals(QuestionType.SINGLE_ANSWER.toString())){
			currentQuestionType = QuestionType.SINGLE_ANSWER;
			createRadioButtonGroup(question);
		}
		// setting options for true-false questions
		if(question.getQuestionType().equals(QuestionType.TRUE_FALSE.toString())){
			currentQuestionType = QuestionType.TRUE_FALSE;
			createRadioButtonGroup(question);
		}
		// setting edittext to write the answer
		if(question.getQuestionType().equals(QuestionType.FILL_IN_THE_BLANKS.toString())){
			currentQuestionType = QuestionType.FILL_IN_THE_BLANKS;
			prepareEditText(getString(R.string.edittext_default_text));
		}
		// do nothing for all other cases
		if(question.getQuestionType().equalsIgnoreCase("")){
			
		}
//		currentQuestionId = questionToShow;
	}
	
	/**
	 * To show the next question.
	 */
	public void showNextQuestion(){
		// incrementing current question count by 1
		currentQuestionNo++;
		QuizActivity.currentQuestion = QuizActivity.allQuestions.get(currentQuestionNo - 1);
		setPageContents(QuizActivity.currentQuestion);
		QuizActivity.currentLinkText = getString(R.string.link_text_answer);
		link.setText(QuizActivity.currentLinkText);
		layoutQuestion.setVisibility(View.VISIBLE);
		layoutEvaluation.setVisibility(View.GONE);
		setContentView(activityQuizLayout);
		isEvaluationShowing = false;
		isAnswerShowing = false;
	}
	
	/**
	 * To take the user back to the menu screen
	 */
	public void goBackToMenu(){
		finish();
	}
	
	/**
	 * To get the answer user gave before moving to the next question.
	 * @return answer
	 */
	public String getUserAnswer(){
		answer = "";
		// getting answer for multiple choice questions
		if(currentQuestionType == QuestionType.MULTIPLE_ANSWER){
			answerRadioGroup = (RadioGroup)activityQuizLayout.findViewById(R.id.radioGroup_answer);
			CheckBox checkBox;
			// count of selected answer
			int checkBoxSelected = answerRadioGroup.getChildCount();
			for(int i=0; i<checkBoxSelected; i++){
				checkBox = (CheckBox)answerRadioGroup.getChildAt(i);
				if(checkBox.isChecked()){
					answer = answer.concat(checkBox.getText().toString());
//					if(i != (checkBoxSelected-2)){
						answer = answer.concat(", ");
//					}
				}
			}
			// forcing user to select at least one answer
			if(answer.equals("")){
				Toast.makeText(this, getString(R.string.message_select_atleast_one_answer), Toast.LENGTH_LONG).show();
			}
			else{
				userAnswer.put(currentQuestionNo, answer);
			}
			return answer;
		}
		// getting answer for single choice/true-false questions
		if(currentQuestionType == QuestionType.SINGLE_ANSWER
				|| currentQuestionType == QuestionType.TRUE_FALSE){
			answerRadioGroup = (RadioGroup)activityQuizLayout.findViewById(R.id.radioGroup_answer);
			// id of the selected answer
			int checkedButtonId = answerRadioGroup.getCheckedRadioButtonId();
			// forcing user to select an answer
			if(checkedButtonId == -1){
				Toast.makeText(this, getString(R.string.message_select_exactly_one_answer), Toast.LENGTH_LONG).show();
			}
			else{
				RadioButton selectedRadioButton = (RadioButton)answerRadioGroup.getChildAt(checkedButtonId);
				answer = selectedRadioButton.getText().toString();
				userAnswer.put(currentQuestionNo, answer);
			}
			return answer;
		}
		// getting answer for fill in blanks type questions
		if(currentQuestionType == QuestionType.FILL_IN_THE_BLANKS){
			answer = answerEditText.getText().toString();
			// forcing user to answer
			if(answer.equals("") || answer.equals(getString(R.string.edittext_default_text))){
				Toast.makeText(this, "You must answer this question.", Toast.LENGTH_LONG).show();
			}
			else{
				userAnswer.put(currentQuestionNo, answer);
			}
			return answer;
		}
		return answer;
	}
	
	/**
	 * To show the answer of the current question
	 * @param userAnswer
	 */
	public void showAnswer(String userAnswer){
		// show appropriate message and emoticon for correct answer
		if(isAnswerCorrect(userAnswer, QuizActivity.currentQuestion.getCorrectAnswers())){
			textviewEvaluationText.setText(R.string.message_answer_correct);
			imageviewEmoticon.setImageResource(R.drawable.answer_correct);
			countCorrectAnswer++;
		}
		// show appropriate message and emoticon for wrong answer
		else{
			textviewEvaluationText.setText(R.string.message_answer_wrong);
			imageviewEmoticon.setImageResource(R.drawable.answer_incorrect);
		}
		// if current question is the last one, clicking the bottom link next time
		// should show the final evaluation
		if(currentQuestionNo == QuizActivity.allQuestions.size()){
			QuizActivity.currentLinkText = getString(R.string.link_text_show_evaluation);
		}
		else{
			QuizActivity.currentLinkText = getString(R.string.link_text_next_question);
		}
		link.setText(QuizActivity.currentLinkText);
		textviewEvaluationTitle.setText(R.string.title_answer);
		textviewEvaluationComment.setText(QuizActivity.currentQuestion.getComment());
		
		layoutQuestion.setVisibility(View.GONE);
		layoutEvaluation.setVisibility(View.VISIBLE);
		isEvaluationShowing = false;
		isAnswerShowing = true;
	}
	
	/**
	 * To show the fianl evaluation.
	 */
	public void showFinalEvaluation(){
		// creating the evaluation message
		String evaluationResult = getString(R.string.message_evaluation_result_1) 
				+ " " + countCorrectAnswer + " " 
				+ getString(R.string.message_evaluation_result_2) 
				+ " " + QuizActivity.allQuestions.size() + " " 
				+ getString(R.string.message_evaluation_result_3);
		
		// Answer to 0 questions were correct
		if(QuizActivity.allQuestions.size() - countCorrectAnswer == QuizActivity.allQuestions.size()){
			textviewEvaluationComment.setText(getString(R.string.text_evaluation_comment_1));
			imageviewEmoticon.setImageResource(R.drawable.smiley_happy_0);

		}
		// Answer to 1 questions were correct
		else if(QuizActivity.allQuestions.size() - countCorrectAnswer == 2){
			textviewEvaluationComment.setText(getString(R.string.text_evaluation_comment_2));
			imageviewEmoticon.setImageResource(R.drawable.smiley_happy_1);

		}
		// Answer to 2 questions were correct
		else if(QuizActivity.allQuestions.size() - countCorrectAnswer == 1){
			textviewEvaluationComment.setText(getString(R.string.text_evaluation_comment_3));
			imageviewEmoticon.setImageResource(R.drawable.smiley_happy_2);
		}
		// Answer to all questions were correct
		else{
			textviewEvaluationComment.setText(getString(R.string.text_evaluation_comment_4));
			imageviewEmoticon.setImageResource(R.drawable.smiley_happy_3);
		}
		textviewEvaluationText.setText(evaluationResult);
		textviewEvaluationTitle.setVisibility(View.INVISIBLE);
		layoutQuestion.setVisibility(View.GONE);
		layoutEvaluation.setVisibility(View.VISIBLE);
		isEvaluationShowing = true;
		isAnswerShowing = false;
	}
	
	/**
	 * To check if the user answer is correct or not.
	 * @param userAnswer
	 * @param correctAnswer
	 * @return boolean
	 */
	public boolean isAnswerCorrect(String userAnswer, String correctAnswer){
		List<String> userAnswerList = new ArrayList<String>();
		String[] userAnswers = userAnswer.split(",");
		String[] correctAnswers = correctAnswer.split(",");
		for(int i=0; i<userAnswers.length; i++){
			if(!userAnswers[i].equals(" ") && userAnswers[i] != null){
				userAnswerList.add(userAnswers[i]);
			}
		}
		// return false if number of user answers and number of correct
		// answers does not match
		if(userAnswerList.size() != correctAnswers.length){
			return false;
		}
		else{
			for(int i=0; i<userAnswerList.size(); i++){
				if(!userAnswerList.get(i).trim().equalsIgnoreCase(correctAnswers[i].trim())){
					return false;
				}
			}
			return true;
		}
	}
	/**
	 * To create a checkbox group for MCQ questions.
	 */
	public void createCheckBoxGroup(Question question){
		// make the radio group visible
		if(scrollViewRadioGroup.getVisibility() == View.GONE){
			scrollViewRadioGroup.setVisibility(View.VISIBLE);
			answerEditText.setVisibility(View.GONE);
			hideSoftKeyboard();
		}
		answerRadioGroup.clearCheck();
		answerRadioGroup.removeAllViews();
		String[] possibleAnswers = question.getPossibleAnswers().split(",");
		CheckBox checkBox;
		for(int i=0; i<possibleAnswers.length; i++){
			checkBox = new CheckBox(this);
			checkBox.setId(i);
			checkBox.setText(possibleAnswers[i].trim());
			answerRadioGroup.addView(checkBox);
		}
	}
		
	/**
	 * To create a radio button group for single choice questions
	 * or true/false questions.
	 */
	public void createRadioButtonGroup(Question question){
		// make the radio group visible
		if(scrollViewRadioGroup.getVisibility() == View.GONE){
			scrollViewRadioGroup.setVisibility(View.VISIBLE);
			answerEditText.setVisibility(View.GONE);
			hideSoftKeyboard();
		}
		answerRadioGroup.clearCheck();
		answerRadioGroup.removeAllViews();
		String[] possibleAnswers = question.getPossibleAnswers().split(",");
		RadioButton radioButton;
		for(int i=0; i<possibleAnswers.length; i++){
			radioButton = new RadioButton(this);
			radioButton.setId(i);
			radioButton.setText(possibleAnswers[i].trim());
			System.out.println(possibleAnswers[i]);
			answerRadioGroup.addView(radioButton);
		}
		
	}
	
	/**
	 * To create a edittext for descriptive answer.
	 */
	public void prepareEditText(String defaultText){
//		if(answerEditText.getVisibility() == View.GONE){
			scrollViewRadioGroup.setVisibility(View.GONE);
			answerEditText.setText(defaultText);
			answerEditText.setVisibility(View.VISIBLE);
			setOnFocusListener(answerEditText);
//		}
	}

	/**
	 * To hide the keyboard if it's showed
	 */
	public void hideSoftKeyboard() {
	    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(answerEditText.getWindowToken(), 0, null);
//	    inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}
	
	/**
	 * To set on focus listener for edittext
	 * @param view
	 */
	private void setOnFocusListener(View view){
		view.setOnFocusChangeListener(new View.OnFocusChangeListener(){

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(v.getId() == R.id.editText_answer && hasFocus){
					answerEditText.setText(currentTextInEditText);
				}
				else{
					if(!currentTextInEditText.equals("")){
						answerEditText.setText(currentTextInEditText);
					}
					else{
						answerEditText.setText(getString(R.string.edittext_default_text));
					}
				}
				
			}});
	}
	
	/**
	 * To initialize all the views
	 */
	private void initializeViews(){
		layoutQuestion = (RelativeLayout)activityQuizLayout.findViewById(R.id.layout_question);
		layoutEvaluation = (RelativeLayout)activityQuizLayout.findViewById(R.id.layout_evaluation);
		
		textviewPageTitle = (TextView)activityQuizLayout.findViewById(R.id.textView_quiz_page_title);
		textviewQuestion = (TextView)activityQuizLayout.findViewById(R.id.textView_question);
		textviewEvaluationTitle = (TextView)activityQuizLayout.findViewById(R.id.textView_evaluation_title);
		textviewEvaluationText = (TextView)activityQuizLayout.findViewById(R.id.textView_evaluation_text);
		textviewEvaluationComment = (TextView)activityQuizLayout.findViewById(R.id.textView_evaluation_comment);
		link = (TextView)activityQuizLayout.findViewById(R.id.textView_link);
		imageviewEmoticon = (ImageView)activityQuizLayout.findViewById(R.id.imageView_evaluation_emoticon);
//		videoviewEmoticon = (VideoView)activityQuizLayout.findViewById(R.id.videoView_emoticon);
		answerEditText = (EditText)activityQuizLayout.findViewById(R.id.editText_answer);
		scrollViewRadioGroup = (ScrollView)activityQuizLayout.findViewById(R.id.scrollView_radiogroup);
//		scrollViewResult = (ScrollView)activityQuizLayout.findViewById(R.id.scrollView_result);
		answerRadioGroup = (RadioGroup)activityQuizLayout.findViewById(R.id.radioGroup_answer);
		answerEditText = (EditText)activityQuizLayout.findViewById(R.id.editText_answer);
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig){
		super.onConfigurationChanged(newConfig);
		
    	savedRadioGroup = (RadioGroup)findViewById(R.id.radioGroup_answer);
    	currentTextInEditText = answerEditText.getText().toString();
    	System.out.println(currentTextInEditText);
    	if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
    		MainActivity.currentOrientation = Configuration.ORIENTATION_LANDSCAPE;
    		activityQuizLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.quiz_layout_land, 
    				new RelativeLayout(this), false);
        }

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
        	MainActivity.currentOrientation = Configuration.ORIENTATION_PORTRAIT;
        	activityQuizLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.quiz_layout_port, 
        			new RelativeLayout(this), false);
        	
        }
        initializeViews();
        textviewPageTitle.setText(R.string.title_quiz_page);
        if(currentQuestionType == QuestionType.MULTIPLE_ANSWER ||
        		currentQuestionType == QuestionType.SINGLE_ANSWER ||
        		currentQuestionType == QuestionType.TRUE_FALSE){
        	ViewGroup previousRadioGroupParent = (ViewGroup) savedRadioGroup.getParent();
        	previousRadioGroupParent.removeView(savedRadioGroup);
        	
        	answerRadioGroup = (RadioGroup)activityQuizLayout.findViewById(R.id.radioGroup_answer);
        	ViewGroup currentRadioGroupParent = (ViewGroup)answerRadioGroup.getParent();
            int index = currentRadioGroupParent.indexOfChild(answerRadioGroup);
            currentRadioGroupParent.removeView(answerRadioGroup);
            currentRadioGroupParent.addView(savedRadioGroup, index);
        }
        if(currentQuestionType == QuestionType.FILL_IN_THE_BLANKS){
        	prepareEditText(currentTextInEditText);
        }

    	link.setText(QuizActivity.currentLinkText);
        if(isEvaluationShowing){
        	showFinalEvaluation();
        }
        else if(isAnswerShowing){
        	showAnswer(QuizActivity.answer);
        }
        
   		textviewQuestion.setText(questionText);
    	setContentView(activityQuizLayout);
    	 
    }
}
