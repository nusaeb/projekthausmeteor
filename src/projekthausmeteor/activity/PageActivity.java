package projekthausmeteor.activity;

import java.util.List;

import projekthausmeteor.data.dao.PageContentDataSource;
import projekthausmeteor.data.model.PageContent;
import projekthausmeteor.listener.OnSwipeTouchListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.projekthausmeteor.R;


public class PageActivity extends Activity {

	private static List<PageContent> allPageContents;
	public static PageContent currentPageContent;
	public static int currentPageNum = 1;
	public static int currentAreaId = 1;
	public static boolean directionShowed = false;
	private Context context;
	private LinearLayout layoutPageIndicator;
	private RelativeLayout layoutPageContent, layoutPopup, layoutPopupContent, layoutWithThreeImage;
	private RelativeLayout layoutRoot, layoutAfterTourOption;
	private PopupWindow popup;
	private TextView mainText, pageTitle;
	private ImageView mainImage, pageIndicator1, pageIndicator2, pageIndicator3, 
		pageIndicator4, pageIndicator5, pageIndicator6, pageIndicator7;
	private PageContentDataSource pageContentDataSource;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        MainActivity.currentOrientation = this.getResources().getConfiguration().orientation;
        if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
        	layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land, 
        			new RelativeLayout(this), false);
//        	layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land_design_1, 
//        			new RelativeLayout(this), false);
        	//mainImage = new ImageView(this);
        	
        	
        }
        else{
        	layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_port, 
        			new RelativeLayout(this), false);
//        	mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
        }
        
        initializeViews();
        pageContentDataSource = new PageContentDataSource(context);
        pageContentDataSource.open();
		
        Intent intent = getIntent();
        int pageId = intent.getIntExtra("pageId", 0);
        currentAreaId = intent.getIntExtra("areaId", 1);
        // Specific laboratory was chosen from the list
        if(pageId != 0){
        	// getting the specific laboratory content by id
        	PageActivity.currentPageContent = pageContentDataSource.getPageContentByPageId(pageId);
        	layoutPageIndicator = (LinearLayout)layoutRoot.findViewById(R.id.layout_page_indicator);
        	layoutPageIndicator.setVisibility(View.INVISIBLE);
        }
        // Start tour from beginning
        else{
        	PageActivity.allPageContents = 
    				pageContentDataSource.getAllPageContentsByAreaId(currentAreaId);
        	currentPageNum = 1;
        	PageActivity.currentPageContent = PageActivity.allPageContents.get(PageActivity.currentPageNum-1);
        	setSwipeListener();
        }
        pageContentDataSource.close();
        setPageContents(PageActivity.currentPageContent);
        setContentView(layoutRoot);
        
        
    }
	
	@Override
	protected void onResume(){
		super.onResume();
		if(PageActivity.directionShowed == true){
//			PageActivity.currentPageContent = PageActivity.allPageContents.get(PageActivity.currentPageNum-1);
//	    	setSwipeListener();
//	    	setPageContents(PageActivity.currentPageContent);
//	    	setContentView(layoutRoot);
//	    	PageActivity.directionShowed = false;
		}
		
	}
    
	/**
	 * To set the swipe listener for changing pages
	 */
    public void setSwipeListener(){
    	layoutRoot.setOnTouchListener(new OnSwipeTouchListener(this){
    	    public void onSwipeRight() {
    	    	if(PageActivity.currentPageNum > 1 && 
    	    			PageActivity.currentPageNum <= PageActivity.allPageContents.size()+1){
    	    		
    	    		PageActivity.currentPageNum--;
    	    		/*if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE && 
    	    				PageActivity.currentPageNum == 1){
    	    			layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land_design_1, 
    	            			new RelativeLayout(context), false);
    	    			mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
    	            	setSwipeListener();
    	            	PageActivity.currentPageContent = PageActivity.allPageContents.get((PageActivity.currentPageNum - 1));
        	    		//setPageContents(PageActivity.currentPageContent);
        	    		setContentView(layoutRoot);
    	    		}
    	    		else if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE && 
    	    				PageActivity.currentPageNum == 2){
    	    			layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land_design_2, 
    	            			new RelativeLayout(context), false);
    	    			mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
    	            	setSwipeListener();
    	            	PageActivity.currentPageContent = PageActivity.allPageContents.get((PageActivity.currentPageNum - 1));
        	    		//setPageContents(PageActivity.currentPageContent);
        	    		setContentView(layoutRoot);
    	    		}
    	    		else{
    	    			layoutPageContent.setVisibility(View.VISIBLE);
    	    			layoutAfterTourOption.setVisibility(View.GONE);
    	    			PageActivity.currentPageContent = PageActivity.allPageContents.get(
        	    				(PageActivity.currentPageNum - 1));
        	    		setPageContents(PageActivity.currentPageContent);
    	    		}*/
    	    		if(PageActivity.currentPageNum == PageActivity.allPageContents.size()){
    	    			layoutPageContent.setVisibility(View.VISIBLE);
    	    			layoutAfterTourOption.setVisibility(View.GONE);
    	    		}
    	    		PageActivity.currentPageContent = PageActivity.allPageContents.get(
    	    				(PageActivity.currentPageNum - 1));
    	    		setPageContents(PageActivity.currentPageContent);
    	    		setContentView(layoutRoot);
    	    	}
    	    }
    	    public void onSwipeLeft() {
    	    	if(PageActivity.currentPageNum > 0 && 
    	    			PageActivity.currentPageNum <= PageActivity.allPageContents.size()){
    	    		
    	    		PageActivity.currentPageNum++;
    	    		if(PageActivity.currentPageNum == PageActivity.allPageContents.size()+1){
    	    			if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
    	    				layoutRoot.setBackgroundColor(getResources().getColor(R.color.white));
    	    			}
    	    			pageTitle.setText(R.string.title_on_tour_complete_area_1);
    	        		layoutPageContent.setVisibility(View.GONE);
    	        		layoutAfterTourOption.setVisibility(View.VISIBLE);
    	        		if(currentAreaId >= 5){
    	        			TextView buttonNextTourPoint = (TextView)layoutRoot.findViewById(R.id.textView_next_tour_point);
    	            		buttonNextTourPoint.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home, 0, 0);
    	            		buttonNextTourPoint.setText(R.string.button_go_to_main_menu);
    	            	}
    	        		
    	        		managePageIndicator();
    	    		}
    	    		/*else if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE && 
    	    				PageActivity.currentPageNum == 1){
    	    			layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land_design_1, 
    	            			new RelativeLayout(context), false);
    	    			mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
    	            	setSwipeListener();
    	            	PageActivity.currentPageContent = PageActivity.allPageContents.get((PageActivity.currentPageNum - 1));
        	    		//setPageContents(PageActivity.currentPageContent);
        	    		setContentView(layoutRoot);
    	    		}
    	    		else if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE && 
    	    				PageActivity.currentPageNum == 2){
    	    			layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land_design_2, 
    	            			new RelativeLayout(context), false);
    	    			mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
    	            	setSwipeListener();
    	            	PageActivity.currentPageContent = PageActivity.allPageContents.get((PageActivity.currentPageNum - 1));
        	    		//setPageContents(PageActivity.currentPageContent);
        	    		setContentView(layoutRoot);
    	    		}
    	    		else if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE && 
    	    				PageActivity.currentPageNum == 3){
//    	    			layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land, 
//    	            			new RelativeLayout(context), false);
////    	            	mainImage = new ImageView(context);
//    	            	initializeViews();
//    	            	setSwipeListener();
    	            	PageActivity.currentPageContent = PageActivity.allPageContents.get((PageActivity.currentPageNum - 1));
        	    		setPageContents(PageActivity.currentPageContent);
        	    		setContentView(layoutRoot);
    	    		}*/
    	    		else{
//    	    			layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land, 
//    	            			new RelativeLayout(context), false);
//    	            	mainImage = new ImageView(context);
    	            	initializeViews();
    	            	setSwipeListener();
    	    			PageActivity.currentPageContent = PageActivity.allPageContents.get((PageActivity.currentPageNum - 1));
        	    		setPageContents(PageActivity.currentPageContent);
        	    		setContentView(layoutRoot);
        	    		
    	    		}
    	    	}
    	    } 
    	});
    }
    
    /**
     * To set the page contents.
     * @param pageContent
     */
    public void setPageContents(PageContent pageContent){
    	String pageText = getString(pageContent.getPageTextId());
    	pageTitle.setText(pageContent.getPageTitleId());
		mainText.setText(pageText);
		mainImage.setImageResource(pageContent.getPageImageId());
		// setting font size based on the length of the text
		if(pageText.length() < 500){
			mainText.setTextSize(13f);
		}
		else{
			mainText.setTextSize(12f);
		}
//    	if(MainActivity.currentOrientation == Configuration.ORIENTATION_PORTRAIT){
//    		ImageView mainImage = (ImageView) layoutRoot.findViewById(R.id.imageView_main_image);
//    		mainImage.setImageResource(pageContent.getPageImageId());
//    	}
//    	else if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE && PageActivity.currentPageNum == 1){
//    		ImageView mainImage = (ImageView) layoutRoot.findViewById(R.id.imageView_main_image);
//    		mainImage.setImageResource(pageContent.getPageImageId());
//    	}
//    	else if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
//        	layoutRoot.setBackgroundResource(pageContent.getPageImageId());
//    	}
    	TextView linkMoreInfo = (TextView)layoutRoot.findViewById(R.id.textView_link_more_info);
//    	TextView linkShowDirection = (TextView)layoutRoot.findViewById(R.id.textView_link_show_direction);
    	// making the link for more information visible/invisible for current page
    	if(pageContent.getHasPopup() != 0){
    		linkMoreInfo.setVisibility(View.VISIBLE);
    	}
    	else{
    		linkMoreInfo.setVisibility(View.INVISIBLE);
    	}
    	// making the link for showing direction invisible if last page, 
    	// otherwise visible
    	if(PageActivity.currentPageNum == 5){
//    		linkShowDirection.setVisibility(View.GONE);
    	}
    	else{
//    		linkShowDirection.setVisibility(View.VISIBLE);
    	}
    	TextView currentPageNumber = (TextView)layoutRoot.findViewById(R.id.current_page_number);
    	currentPageNumber.setText("" + PageActivity.currentPageNum);
    	// managing page indicator
    	managePageIndicator();
    }
    
    /**
     * To set the popup contents.
     * @param pageNumber
     */
    @SuppressWarnings("deprecation")
	public void setPopupContent(int pageNumber){
		
		layoutPopupContent = (RelativeLayout)layoutPopup.findViewById(R.id.layout_popup_content);
		layoutWithThreeImage = (RelativeLayout)layoutPopup.findViewById(R.id.layout_with_three_image);
		int[] imageIds = PageActivity.currentPageContent.getPagePopupImageId();
		int[] textIds = PageActivity.currentPageContent.getPagePopupTextId();
		// if popup has three images
		if(imageIds != null && imageIds.length == 3){
			layoutPopupContent.setVisibility(View.GONE);
			layoutWithThreeImage.setVisibility(View.VISIBLE);
			
			String[] popupTexts = getString(textIds[0]).split(",");
			
			TextView popupText_1 = (TextView)layoutPopup.findViewById(R.id.textView_text_image_1);
//			ImageView popupImage_1 = (ImageView)layoutPopup.findViewById(R.id.imageView_popup_image_1);
			popupText_1.setText(popupTexts[0]);
			popupText_1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, imageIds[0]);
//			popupImage_1.setImageResource(imageIds[0]);
			
			TextView popupText_2 = (TextView)layoutPopup.findViewById(R.id.textView_text_image_2);
//			ImageView popupImage_2 = (ImageView)layoutPopup.findViewById(R.id.imageView_popup_image_2);
			popupText_2.setText(popupTexts[1]);
			popupText_2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, imageIds[1]);
//			popupImage_2.setImageResource(imageIds[1]);
			
			TextView popupText_3 = (TextView)layoutPopup.findViewById(R.id.textView_text_image_3);
//			ImageView popupImage_3 = (ImageView)layoutPopup.findViewById(R.id.imageView_popup_image_3);
			popupText_3.setText(popupTexts[2]);
			popupText_3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, imageIds[2]);
//			popupImage_3.setImageResource(imageIds[2]);
		}
		else if(imageIds == null){
			layoutPopupContent.setVisibility(View.VISIBLE);
			layoutWithThreeImage.setVisibility(View.GONE);
			TextView popupTextTop = (TextView)layoutPopup.findViewById(R.id.textView_popup_text_top);
			TextView popupTextBottom = (TextView)layoutPopup.findViewById(R.id.textView_popup_text_bottom);
			ImageView popupImage = (ImageView)layoutPopup.findViewById(R.id.imageView_popup_image);
			popupImage.setVisibility(View.GONE);
			popupTextBottom.setVisibility(View.GONE);
			popupTextTop.setText(textIds[0]);
			popupTextTop.setLayoutParams(new RelativeLayout.LayoutParams
					(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		}
		else if(PageActivity.currentAreaId == 3){
			Toast.makeText(context, "h", Toast.LENGTH_SHORT).show();
			layoutPopupContent.setVisibility(View.VISIBLE);
			layoutWithThreeImage.setVisibility(View.GONE);
			TextView popupTextTop = (TextView)layoutPopup.findViewById(R.id.textView_popup_text_top);
			TextView popupTextBottom = (TextView)layoutPopup.findViewById(R.id.textView_popup_text_bottom);
			ImageView popupImage = (ImageView)layoutPopup.findViewById(R.id.imageView_popup_image);
			popupTextTop.setVisibility(View.GONE);
			popupTextBottom.setVisibility(View.VISIBLE);
			popupTextBottom.setText(textIds[0]);
			popupImage.setImageResource(imageIds[0]);
			popupTextBottom.setLayoutParams(new RelativeLayout.LayoutParams
					(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		}
		else{
			layoutPopupContent.setVisibility(View.VISIBLE);
			layoutWithThreeImage.setVisibility(View.GONE);
			TextView popupTextTop = (TextView)layoutPopup.findViewById(R.id.textView_popup_text_top);
			TextView popupTextBottom = (TextView)layoutPopup.findViewById(R.id.textView_popup_text_bottom);
			ImageView popupImage = (ImageView)layoutPopup.findViewById(R.id.imageView_popup_image);
			popupTextTop.setText(textIds[0]);
			popupImage.setImageResource(imageIds[0]);
			popupTextBottom.setVisibility(View.GONE);
		}
		        
	}
    
    /**
     * To manage the page indicator
     */
    private void managePageIndicator(){
    	if(PageActivity.currentPageNum == 1){
    		pageIndicator1.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator2.setImageResource(R.drawable.page_indicator_inactive);
    	}
    	else if(PageActivity.currentPageNum == 2){
    		pageIndicator1.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator2.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator3.setImageResource(R.drawable.page_indicator_inactive);
    	}
    	else if(PageActivity.currentPageNum == 3){
    		pageIndicator2.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator3.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator4.setImageResource(R.drawable.page_indicator_inactive);
    	}
    	else if(PageActivity.currentPageNum == 4){
    		pageIndicator3.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator4.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator5.setImageResource(R.drawable.page_indicator_inactive);
    	}
    	else if(PageActivity.currentPageNum == 5){
    		pageIndicator4.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator5.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator6.setImageResource(R.drawable.page_indicator_inactive);
    	}
    	else if(PageActivity.currentPageNum == 6){
    		pageIndicator5.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator6.setImageResource(R.drawable.page_indicator_active);
//    		pageIndicator7.setImageResource(R.drawable.page_indicator_inactive);
    	}
    	else if(PageActivity.currentPageNum == 7){
    		pageIndicator6.setImageResource(R.drawable.page_indicator_inactive);
//    		pageIndicator7.setImageResource(R.drawable.page_indicator_active);
    	}
    	if(PageActivity.allPageContents.size() <= 4){
    		pageIndicator6.setVisibility(View.GONE);
    	}
    }

    /**
     * To show popup onclick of a link
     * @param view
     */
	public void showPopup(View view){
		
		DisplayMetrics metrics;
		int popupWidth, popupHeight;
		if (MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
    		metrics = getApplicationContext().getResources().getDisplayMetrics();
        	popupWidth = (int)(metrics.widthPixels);
        	popupHeight = (int)(metrics.heightPixels * 0.80);
        	layoutPopup = (RelativeLayout)getLayoutInflater().inflate(R.layout.popup_layout_land, 
    				new RelativeLayout(context), false);
        	setPopupContent(PageActivity.currentPageNum);
    		popup = new PopupWindow(layoutPopup, popupWidth, popupHeight, true);
    		popup.setAnimationStyle(R.style.PopupAnimation2);
    		popup.showAtLocation(layoutRoot, Gravity.CENTER, 0, 0);
    	}
		else{
			metrics = getApplicationContext().getResources().getDisplayMetrics();
	    	popupWidth = (int)(metrics.widthPixels);
	    	popupHeight = (int)(metrics.heightPixels * 0.85);
	    	layoutPopup = (RelativeLayout)getLayoutInflater().inflate(R.layout.popup_layout_port, 
					new RelativeLayout(context), false);
	    	setPopupContent(PageActivity.currentPageNum);
	    	popup = new PopupWindow(layoutPopup, popupWidth, popupHeight, true);
    		popup.setAnimationStyle(R.style.PopupAnimation2);
    		popup.showAtLocation(layoutRoot, Gravity.CENTER, 0, 20);
		}
		

	}
	
//	/**
//	 * To show direction to another room onclick of a link
//	 * @param view
//	 */
//	public void showDirectionToRoom(View view){
//		
//		Intent directionVideoIntent = new Intent(this, DirectionVideoActivity.class);
//		directionVideoIntent.putExtra("fromRoomId", PageActivity.currentPageNum);
//		directionVideoIntent.putExtra("toRoomId", PageActivity.currentPageNum+1);
//		this.startActivity(directionVideoIntent);
//	}
	
	/**
	 * To take user back to home screen
	 * @param view
	 */
	public void goToHome(View view){
		finish();
	}
	
	/**
	 * To start the next tour onclick of a button
	 * @param view
	 */
    public void goToNextTourPoint(View view){
    	if(currentAreaId < 5){
    		Intent directionVideoIntent = new Intent(this, DirectionVideoActivity.class);
    		directionVideoIntent.putExtra("fromAreaId", currentAreaId);
    		directionVideoIntent.putExtra("toAreaId", currentAreaId + 1);
    		this.startActivity(directionVideoIntent);
    	}
    	else{
    		Intent mainActivityIntent = new Intent(context, MainActivity.class);
    		mainActivityIntent.putExtra("showScreen", 4);
    		this.startActivity(mainActivityIntent);
    		
//    		AlertDialog.Builder dialog = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
//    		dialog.setMessage(R.string.dialog_message_next_section_unavailable);
//			dialog.setNeutralButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//					
//				}
//				
//			});
//			dialog.show();
    	}
    	
    }
    
    /**
     * To start quiz onclick of a button
     * @param view
     */
    public void startQuiz(View view){
    	Intent quizIntent = new Intent(context, QuizActivity.class);
		this.startActivity(quizIntent);
    }
    
    /**
     * To close popup onclick of a button
     * @param view
     */
    public void closePopup(View view){
    	if(popup.isShowing()){
    		popup.dismiss();
    	}
	}
    
    /**
	 * To initialize all the views
	 */
	private void initializeViews(){
		// initializing the layout that contains the main contents(text, image etc.)
		layoutPageContent = (RelativeLayout)layoutRoot.findViewById(R.id.layout_page_content);
		// initializing the layout that contains the button showed after one tour finishes
        layoutAfterTourOption = (RelativeLayout)layoutRoot.findViewById(R.id.layout_option_showed_after_tour);
        
        //initializing the page title, main text and main image views
        pageTitle = (TextView)layoutRoot.findViewById(R.id.textView_page_title);
    	mainText = (TextView)layoutRoot.findViewById(R.id.textView_main_text);
    	mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
    	
    	// initializing the page indicator images
    	pageIndicator1 = (ImageView)layoutRoot.findViewById(R.id.imageView_page_indicator_1);
        pageIndicator2 = (ImageView)layoutRoot.findViewById(R.id.imageView_page_indicator_2);
        pageIndicator3 = (ImageView)layoutRoot.findViewById(R.id.imageView_page_indicator_3);
        pageIndicator4 = (ImageView)layoutRoot.findViewById(R.id.imageView_page_indicator_4);
        pageIndicator5 = (ImageView)layoutRoot.findViewById(R.id.imageView_page_indicator_5);
        pageIndicator6 = (ImageView)layoutRoot.findViewById(R.id.imageView_page_indicator_6);
//        pageIndicator7 = (ImageView)layoutRoot.findViewById(R.id.imageView_page_indicator_7);
	}
	
    @Override
    public void onConfigurationChanged(Configuration newConfig){
    	super.onConfigurationChanged(newConfig);
    	
//    	if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE && PageActivity.currentPageNum == 1) {
//    		MainActivity.currentOrientation = Configuration.ORIENTATION_LANDSCAPE;
//    		layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land_design_1, 
//    				new RelativeLayout(this), false);
////    		mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
//    		setSwipeListener();
//    		setContentView(layoutRoot);
//    		return;
//    	}
//    	else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE && PageActivity.currentPageNum == 2) {
//    		MainActivity.currentOrientation = Configuration.ORIENTATION_LANDSCAPE;
//    		layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land_design_2, 
//    				new RelativeLayout(context), false);
////    		mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
//    		setSwipeListener();
//    		setContentView(layoutRoot);
//    		return;
//        }
    	if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
    		MainActivity.currentOrientation = Configuration.ORIENTATION_LANDSCAPE;
    		layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_land, 
    				new RelativeLayout(this), false);
        	mainImage = new ImageView(this);
        }
    	else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
        	MainActivity.currentOrientation = Configuration.ORIENTATION_PORTRAIT;
        	layoutRoot = (RelativeLayout)getLayoutInflater().inflate(R.layout.page_layout_port, 
        			new RelativeLayout(this), false);
        	mainImage = (ImageView)layoutRoot.findViewById(R.id.imageView_main_image);
        }
        initializeViews();
    	
    	if(PageActivity.currentPageNum == PageActivity.allPageContents.size()+1){
			if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
				layoutRoot.setBackgroundColor(getResources().getColor(R.color.white));
			}
			pageTitle.setText(R.string.title_on_tour_complete_area_1);
    		layoutPageContent.setVisibility(View.GONE);
    		layoutAfterTourOption.setVisibility(View.VISIBLE);
    		managePageIndicator();
		}
		else{
    		setPageContents(PageActivity.currentPageContent);
    		setContentView(layoutRoot);
		}
    	if(popup != null && popup.isShowing()){
    		popup.dismiss();
    		showPopup(new TextView(this));
    	}
    	setSwipeListener();
    	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
