package projekthausmeteor.activity;

import com.example.projekthausmeteor.R;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class PopupActivity extends Activity{

	private RelativeLayout layoutRoot, layoutPopupContent, layoutWithThreeImage;

	private VideoView videoViewPort, videoViewLand;
	private ImageView buttonPlayPort, buttonPlayLand;
	private String videoPath;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Toast.makeText(getApplicationContext(), "oncreate called...", Toast.LENGTH_LONG).show();
		Intent intent = getIntent();
		int popupWidth = intent.getIntExtra("popupWidth", LayoutParams.MATCH_PARENT);
		int popupHeight = intent.getIntExtra("popupHeight", LayoutParams.WRAP_CONTENT);
		int pageNumber = intent.getIntExtra("pageNumber", 0);
		
		WindowManager.LayoutParams params = getWindow().getAttributes();  
		params.x = 0;  
		params.height = popupHeight;  
		params.width = popupWidth;  
		params.y = 0;
		
		
		//videoViewPort = (VideoView)layoutRoot.findViewById(R.id.videoView_popup);
		//videoViewLand = (VideoView)layoutRoot.findViewById(R.id.videoView_popup);
		//buttonPlayPort = (ImageView)layoutRoot.findViewById(R.id.imageView_play_video);
		//buttonPlayLand = (ImageView)layoutRoot.findViewById(R.id.imageView_play_video);
		
		if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
			layoutRoot = (RelativeLayout)getLayoutInflater()
					.inflate(R.layout.popup_layout_land, new RelativeLayout(this), false);
			
        }
        else{
        	layoutRoot = (RelativeLayout)getLayoutInflater()
    				.inflate(R.layout.popup_layout_port, new RelativeLayout(this), false);
        }
		this.getWindow().setAttributes(params);
		setPopupContents(PageActivity.currentPageNum);
		setContentView(layoutRoot);
		//videoView = (VideoView)popupLayout.findViewById(R.id.videoView_popup);
		//videoView = new VideoView(this);
		
		
		
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig){
		
    	if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
    		MainActivity.currentOrientation = Configuration.ORIENTATION_LANDSCAPE;
    		/*popupLayout = (RelativeLayout)getLayoutInflater()
					.inflate(R.layout.popup_content_land, new RelativeLayout(this), false);*/
    		//mainLayout = getLayoutInflater().inflate(R.layout.activity_main_land, new RelativeLayout(this), false);
    		/*setPopupContents(MainActivity.currentPageNum);
    		if(isPlaying){
    			VideoView vide = (VideoView)popupLayout.findViewById(R.id.videoView_popup);
        		int index = popupLayout.indexOfChild(vide);
        		popupLayout.removeViewAt(index);
    			imagePlay = (ImageView)popupLayout.findViewById(R.id.imageView_popup);
    			imagePlay.setVisibility(View.GONE);
    			//videoView.setVisibility(View.VISIBLE);
    			//VideoView video = videoView;
    			popupLayout.addView(videoView, index);
    		}*/
    		layoutRoot = (RelativeLayout)getLayoutInflater()
    				.inflate(R.layout.popup_layout_land, new RelativeLayout(this), false);
    		setPopupContents(PageActivity.currentPageNum);
    		setContentView(layoutRoot);
    		//videoView.start();
        }

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
        	MainActivity.currentOrientation = Configuration.ORIENTATION_PORTRAIT;
        	/*popupLayout = (RelativeLayout)getLayoutInflater()
					.inflate(R.layout.popup_content, new RelativeLayout(this), false);
        	//mainLayout = getLayoutInflater().inflate(R.layout.activity_main, new RelativeLayout(this), false);
        	setPopupContents(MainActivity.currentPageNum);
        	if(isPlaying){
        		VideoView vide = (VideoView)popupLayout.findViewById(R.id.videoView_popup);
        		int index = popupLayout.indexOfChild(vide);
        		popupLayout.removeViewAt(index);
    			imagePlay = (ImageView)popupLayout.findViewById(R.id.imageView_popup);
    			imagePlay.setVisibility(View.GONE);
    			//videoView.setVisibility(View.VISIBLE);
    			//VideoView video = videoView;
    			popupLayout.addView(videoView, index);
    		}*/
        	layoutRoot = (RelativeLayout)getLayoutInflater()
    				.inflate(R.layout.popup_layout_port, new RelativeLayout(this), false);
        	setPopupContents(PageActivity.currentPageNum);
        	setContentView(layoutRoot);
        	//videoView.start();
        }
    	super.onConfigurationChanged(newConfig);
    }
	
	public void playVideo(View view){
		buttonPlayPort.setVisibility(View.GONE);
		buttonPlayLand.setVisibility(View.GONE);
		
		//final VideoView videoView = new VideoView(this);
		//videoView = (VideoView)popupLayout.findViewById(R.id.videoView_popup);
		
		
		final MediaController mediaController = new MediaController(this);
		mediaController.setAnchorView(videoViewPort);
		videoViewPort.setMediaController(mediaController);
		videoViewPort.setVideoPath(videoPath);
		videoViewPort.setVisibility(View.VISIBLE);
		mediaController.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "clicked..", Toast.LENGTH_SHORT).show();
			}
			
		});
		videoViewPort.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
			
		});
		videoViewPort.setOnPreparedListener(new OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                
            	//VideoView vide = (VideoView)popupLayout.findViewById(R.id.videoView_popup);
        		//int index = popupLayout.indexOfChild(vide);
        		//popupLayout.removeView(vide);
    			
    			//videoView.setVisibility(View.VISIBLE);
    			//VideoView video = videoView;
    			//popupLayout.addView(videoView, index);
        		//vide = videoView;
        		//vide.start();
            	videoViewPort.start();
            	
            	mediaController.setAnchorView(videoViewPort);
                //videoView.pause();
                
               
                /*mediaController.show();
                mediaController.bringToFront();*/
                
            }
        });
		
		
		
		videoViewLand.setMediaController(mediaController);
		videoViewLand.setVideoPath(videoPath);
		videoViewLand.setVisibility(View.VISIBLE);
		//mediaController.show(10000);
		videoViewLand.setOnPreparedListener(new OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                
            	//VideoView vide = (VideoView)popupLayout.findViewById(R.id.videoView_popup);
        		//int index = popupLayout.indexOfChild(vide);
        		//popupLayout.removeView(vide);
    			
    			//videoView.setVisibility(View.VISIBLE);
    			//VideoView video = videoView;
    			//popupLayout.addView(videoView, index);
        		//vide = videoView;
        		//vide.start();
            	videoViewLand.start();
            	
            	mediaController.setAnchorView(videoViewLand);
                //videoView.pause();
                
               
                /*mediaController.show();
                mediaController.bringToFront();*/
                
            }
        });
	}
	
	public void closePopup(View view){
		finish();
	}
	
	public void setPopupContents(int pageNumber){
		
		layoutPopupContent = (RelativeLayout)layoutRoot.findViewById(R.id.layout_popup_content);
		layoutWithThreeImage = (RelativeLayout)layoutRoot.findViewById(R.id.layout_with_three_image);
		int[] imageIds = PageActivity.currentPageContent.getPagePopupImageId();
		int[] textIds = PageActivity.currentPageContent.getPagePopupTextId();
		if(imageIds.length == 3){
			layoutPopupContent.setVisibility(View.GONE);
			layoutWithThreeImage.setVisibility(View.VISIBLE);
			TextView popupText_1 = (TextView)layoutRoot.findViewById(R.id.textView_text_image_1);
//			ImageView popupImage_1 = (ImageView)layoutRoot.findViewById(R.id.imageView_popup_image_1);
			popupText_1.setText(textIds[0]);
//			popupImage_1.setBackgroundResource(imageIds[0]);
			TextView popupText_2 = (TextView)layoutRoot.findViewById(R.id.textView_text_image_2);
//			ImageView popupImage_2 = (ImageView)layoutRoot.findViewById(R.id.imageView_popup_image_2);
			popupText_2.setText(textIds[0]);
//			popupImage_2.setBackgroundResource(imageIds[1]);
			TextView popupText_3 = (TextView)layoutRoot.findViewById(R.id.textView_text_image_3);
//			ImageView popupImage_3 = (ImageView)layoutRoot.findViewById(R.id.imageView_popup_image_3);
			popupText_3.setText(textIds[0]);
//			popupImage_3.setBackgroundResource(imageIds[2]);
		}
		else if(imageIds.length == 0){
			layoutPopupContent.setVisibility(View.VISIBLE);
			layoutWithThreeImage.setVisibility(View.GONE);
			TextView popupText = (TextView)layoutRoot.findViewById(R.id.textView_popup_text_top);
			ImageView popupImage = (ImageView)layoutRoot.findViewById(R.id.imageView_popup_image);
			popupText.setText(textIds[0]);
			popupImage.setVisibility(View.GONE);
		}
		else{
			layoutPopupContent.setVisibility(View.VISIBLE);
			layoutWithThreeImage.setVisibility(View.GONE);
			TextView popupText = (TextView)layoutRoot.findViewById(R.id.textView_popup_text_top);
			ImageView popupImage = (ImageView)layoutRoot.findViewById(R.id.imageView_popup_image);
			popupText.setText(textIds[0]);
			popupImage.setBackgroundResource(imageIds[0]);
		}
		
		/*if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
	        popupText = (TextView)layoutRoot.findViewById(R.id.textView_popup_text);
        }
        else{
	        popupText = (TextView)layoutRoot.findViewById(R.id.textView_popup_text);
        }
        if(pageNumber == 1){
        	videoPath = "android.resource://" + getPackageName() + "/" + R.raw.playing_with_nao;
        	popupText.setText(R.string.text_popup_1);
        }
        if(pageNumber == 2){
        	videoPath = "android.resource://" + getPackageName() + "/" + R.raw.playing_with_nao;
        	popupText.setText(R.string.text_popup_2);
        }
        if(pageNumber == 3){
        	videoPath = "android.resource://" + getPackageName() + "/" + R.raw.playing_with_nao;
        	popupText.setText(R.string.text_popup_3);
        }
        popupText.setMovementMethod(new ScrollingMovementMethod());*/
        
	}
	
	

}
