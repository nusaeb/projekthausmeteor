package projekthausmeteor.activity;

import projekthausmeteor.constant.QuestionType;
import projekthausmeteor.data.dao.DirectionVideoDataSource;
import projekthausmeteor.data.dao.PageContentDataSource;
import projekthausmeteor.data.dao.QuestionDataSource;
import projekthausmeteor.data.dbhelper.DirectionVideoSQLiteHelper;
import projekthausmeteor.data.dbhelper.PageContentSQLiteHelper;
import projekthausmeteor.data.dbhelper.QuestionSQLiteHelper;
import projekthausmeteor.listener.GPSTracker;
import projekthausmeteor.listener.OnSwipeTouchListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.projekthausmeteor.R;


public class MainActivity extends Activity {

	private RelativeLayout mainLayout, layoutMenu, layoutMainContent;
	private ImageView homeScreenImage, pageIndicator1, pageIndicator2, pageIndicator3, pageIndicator4;
	private TextView homeScreenText;
	private QuestionDataSource questionDataSource;
	private PageContentDataSource pageContentDataSource;
	private DirectionVideoDataSource directionVideoDataSource;
	
	public static Context context;
	public static int currentOrientation;
	public static int currentScreenNumber;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.context = this;
        MainActivity.currentOrientation = this.getResources().getConfiguration().orientation;
        
        createAndInsertQuestions();
        createAndInsertPageContents();
        createAndInsertDirectionVideos();
        
        // Set the current screen number to 'showScreen' if coming back to menu 
        // after tour finishes, otherwise to 1.
        Intent intent = getIntent();
        currentScreenNumber = intent.getIntExtra("showScreen", 1);
        System.out.println("+++++++++++++" + currentScreenNumber);
        if(MainActivity.currentOrientation == Configuration.ORIENTATION_LANDSCAPE){
        	mainLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.main_layout_land, 
            		new RelativeLayout(this), false);
        }
        else{
        	mainLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.main_layout_port, 
            		new RelativeLayout(this), false);
        }
        homeScreenText = (TextView)mainLayout.findViewById(R.id.textView_home_screen_text);
        homeScreenImage = (ImageView)mainLayout.findViewById(R.id.imageView_home_screen_image);
        layoutMainContent = (RelativeLayout)mainLayout.findViewById(R.id.layout_main_content);
		layoutMenu = (RelativeLayout)mainLayout.findViewById(R.id.layout_menu);
		setScreenContent(MainActivity.currentScreenNumber);
        setSwipeListener();
        setContentView(mainLayout);
    }
    public void setSwipeListener(){
    	mainLayout.setOnTouchListener(new OnSwipeTouchListener(this){
    	    public void onSwipeRight() {
    	    	if(MainActivity.currentScreenNumber > 1 && MainActivity.currentScreenNumber < 5){
    	    		MainActivity.currentScreenNumber--;
    	    		setScreenContent(MainActivity.currentScreenNumber);
    	    	}
    	    }
    	    public void onSwipeLeft() {
    	    	if(MainActivity.currentScreenNumber > 0 && MainActivity.currentScreenNumber < 4){
    	    		MainActivity.currentScreenNumber++;
    	    		setScreenContent(MainActivity.currentScreenNumber);
    	    	}
    	    	/*else if(MainActivity.currentScreenNumber == 3){
    	    		Intent myIntent = new Intent(MainActivity.context, MenuActivity.class);
    	    		MainActivity.context.startActivity(myIntent);
    	    	}*/
    	    } 
    	});
    }
    
    public void setScreenContent(int screenToShow){
    	pageIndicator1 = (ImageView)mainLayout.findViewById(R.id.imageView_page_indicator_1);
        pageIndicator2 = (ImageView)mainLayout.findViewById(R.id.imageView_page_indicator_2);
        pageIndicator3 = (ImageView)mainLayout.findViewById(R.id.imageView_page_indicator_3);
        pageIndicator4 = (ImageView)mainLayout.findViewById(R.id.imageView_page_indicator_4);
    	TextView currentScreen = (TextView)mainLayout.findViewById(R.id.current_home_screen_number);
    	if(screenToShow == 1){
    		homeScreenText.setText(R.string.text_home_screen_1);
    		homeScreenImage.setImageResource(R.drawable.image_home_screen_1);
    		
    		pageIndicator1.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator2.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator3.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator4.setImageResource(R.drawable.page_indicator_inactive);
    		currentScreen.setText("1");
    	}
    	else if(screenToShow == 2){
    		homeScreenText.setText(R.string.text_home_screen_2);
    		homeScreenImage.setImageResource(R.drawable.image_home_screen_2);
    		
    		pageIndicator1.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator2.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator3.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator4.setImageResource(R.drawable.page_indicator_inactive);
    		currentScreen.setText("2");
    	}
    	else if(screenToShow == 3){
    		swapLayout();
    		homeScreenText.setText(R.string.text_home_screen_3);
    		homeScreenImage.setImageResource(R.drawable.image_home_screen_3);
    		
    		pageIndicator1.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator2.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator3.setImageResource(R.drawable.page_indicator_active);
    		pageIndicator4.setImageResource(R.drawable.page_indicator_inactive);
    		currentScreen.setText("3");
    	}
    	else if(screenToShow == 4){
    		swapLayout();
    		pageIndicator1.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator2.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator3.setImageResource(R.drawable.page_indicator_inactive);
    		pageIndicator4.setImageResource(R.drawable.page_indicator_active);
    		currentScreen.setText("4");
    	}
    }
    
    public void startTour(View view){
    	Intent pageViewIntent = new Intent(this, PageActivity.class);
		this.startActivity(pageViewIntent);
    }
    
    public void chooseDestination(View view){
    	Intent chooseDestIntent = new Intent(this, ChooseDestinationActivity.class);
		this.startActivity(chooseDestIntent);
//		Intent myIntent = new Intent(this, IndoorAtlasActivity.class);
//		this.startActivity(myIntent);
    }
    
    public void startQuiz(View view){
    	Intent myIntent = new Intent(this, QuizActivity.class);
		
		this.startActivity(myIntent);
    }
    
    public void swapLayout(){
    	if(MainActivity.currentScreenNumber == 4){
    		layoutMainContent.setVisibility(View.GONE);
    		layoutMenu.setVisibility(View.VISIBLE);
    	}
    	else{
    		layoutMainContent.setVisibility(View.VISIBLE);
    		layoutMenu.setVisibility(View.GONE);
    	}
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig){
    	
    	if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
    		MainActivity.currentOrientation = Configuration.ORIENTATION_LANDSCAPE;
    		mainLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.main_layout_land, 
    				new RelativeLayout(this), false);
        }

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
        	MainActivity.currentOrientation = Configuration.ORIENTATION_PORTRAIT;
        	mainLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.main_layout_port, 
        			new RelativeLayout(this), false);
        }
        homeScreenText = (TextView)mainLayout.findViewById(R.id.textView_home_screen_text);
        homeScreenImage = (ImageView)mainLayout.findViewById(R.id.imageView_home_screen_image);
        layoutMainContent = (RelativeLayout)mainLayout.findViewById(R.id.layout_main_content);
		layoutMenu = (RelativeLayout)mainLayout.findViewById(R.id.layout_menu);
        setSwipeListener();
        setScreenContent(MainActivity.currentScreenNumber);
        setContentView(mainLayout);
    	super.onConfigurationChanged(newConfig);
    }

    private void createAndInsertQuestions(){
    	questionDataSource = new QuestionDataSource(this);
    	questionDataSource.open();
    	questionDataSource.getDbHelper().dropTable(questionDataSource.getDatabase(), QuestionSQLiteHelper.TABLE_QUESTION);
    	questionDataSource.getDbHelper().createTable(questionDataSource.getDatabase(), QuestionSQLiteHelper.TABLE_QUESTION);
    	boolean isEmpty = questionDataSource.isTableEmpty(QuestionSQLiteHelper.TABLE_QUESTION);
    	if(isEmpty){
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Welche der folgenden Komponenten sind die Kernkomponenten des METEORs?", 
        			"Technik, Wissenschaft, Mensch, Organisation, Medien", 
        			"Technik, Mensch, Organisation",
        			"Das Projekthaus METEOR basiert auf den drei Komponenten Mensch, Technik und Organisation.",
        			1
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.TRUE_FALSE.toString(), 
    				"Der Bau des Projekthauses begann im Jahr 2011 und dauerte insgesamt 2 Jahre.", 
        			"Richtig, Falsch", "Falsch",
        			"Der Bau des Projekthauses begann im Jahr 2009. Im Sommer 2009 erfolgte der Spatenstich.\n " +
        			"Bereits 2 Jahre sp�ter erfolgte die Einweihung und der Einzug in das METEOR.",
        			1
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Welche Lichtfarben werden durch das Lichtsystem des Projekthauses erm�glicht?", 
        			"Neutralwei�, Kaltwei�, Gelbwei�, Tageslichtwei�, Warmwei�", 
        			"Neutralwei�, Tageslichtwei�, Warmwei�",
        			"Die Lichtfarben warmwei�, tageslichtwei� und neutralwei� werden durch das Lichtsystem erm�glicht, was zur positiven Beeinflussung des  Biorhythmus dient und Erm�dungsphasen vorgebeugt.",
        			1
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				" Welche der folgenden Begriffe sind Grundgedanken des Projekthauses?", 
        			"Integrit�t, Modularit�t, Interaktivit�t, Flexibilit�t, Kreativit�t", 
        			"Modularit�t, Flexibilit�t",
        			"Flexibilit�t und Modularit�t sind zwei Grundideen des Projekthauses.",
        			2
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.TRUE_FALSE.toString(), 
    				"Der Begriff Wissensarbeit geht auf den tschechisch-kanadischen Wissenschaftler Peter F. Drucker zur�ck.", 
        			"Richtig, Falsch", 
        			"Falsch",
        			"Der Begriff Wissensarbeit geht auf den �sterreichisch-amerikanischen Wissenschaftler und �konom Peter F. Drucker zur�ck. Er schrieb einflussreiche Werke zu Theorie und Praxis des Managements.",
        			2
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Welche Vorteile ergeben sich aus �Desksharing�?", 
        			"Einsparungen, Weniger Arbeit, Abwechslung, Erh�hte Produktivit�t, K�rzerer Arbeitsweg", 
        			"Einsparungen, Abwechslung, Erh�hte Produktivit�t",
        			"Einige Vorteile des Desksharings sind Einsparungen, Abwechslung und eine erh�hte Produktivit�t.\n" +
					"Wenn diese Methode richtig geplant und umgesetzt wird, k�nnen Arbeitsnehmer sich je nach Aufgabe einen passenden Arbeitsplatz aussuchen, was die genannten Vorteile und weitere positive Effekte mit sich bringt.",
        			2
        			);
    		
    		
    		
    		
    		
    		
    		
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Welche der folgenden Faktoren wurden bei der Umfrage von Union Investment als St�rfaktoren am Arbeitsplatz genannt?", 
        			"Zu laute Ger�uschkulisse, Zu viel Technik, Zu wenig Gr�nfl�chen, Zu viel Platz, Raumtemperatur", 
        			"Raumtemperatur",
        			"Eine zu laute Ger�uschkulisse und die Raumtemperatur waren zwei der zahlreichen St�rfaktoren, die von den Befragten am h�ufigsten genannt wurden. Bei der Umfrage wurden vorrangig organisatorische und menschbezogene Faktoren genannt.",
        			3
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Wie viele Dezibel k�nnen dank den Ohrensesseln im Projekthaus ungef�hr eingespart werden?", 
        			"weniger als 2 dB, ca. 4 dB, mehr als 8 dB", 
        			"ca. 4 dB",
        			"Dank den Ohrensesseln k�nnen ungef�hr 4,19 dB eingespart werden. Dies sorgt f�r eine angenehme Ger�uschkulisse f�r optimale Kommunikation.",
        			3
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.TRUE_FALSE.toString(), 
    				"Das Polyvision Interaktivboard ist eine interaktive Wand, die sich im Innovation Engineering Lab befindet.", 
        			"Richtig, Falsch", 
        			"Richtig",
        			"Das Polyvision Interaktivboard ist eine interaktive Wand, die sich im Competence Lab befindet. Mit dieser Wand k�nnen stets zusammen neue Ideen generiert und diskutiert werden.",
        			3
        			);
    		
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Was bietet das Labor f�r virtuelle Ergonomie?", 
        			"Arbeitsplatzgestaltung, Interaktive Wand, Digitale Menschmodelle, Produktanalysen, Videokonferenzsystem", 
        			"Arbeitsplatzgestaltung, Digitale Menschmodelle, Produktanalysen",
        			"Das Labor f�r virtuelle Ergonomie bietet Forschung zur Arbeitsplatzgestaltung, Produktanalysen und digitale Menschmodelle.",
        			4
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Womit ist das Labor f�r virtuelle Ergonomie unter anderem ausgestattet?", 
        			"CAD-Arbeitspl�tze, Analoges Menschmodell, Digitale Fabrik", 
        			"CAD-Arbeitspl�tze, Digitale Fabrik",
        			"Das Labor ist unter anderem mit CAD-Arbeitspl�tzen und einer digitalen Fabrik ausgestattet. Dies und weitere M�glichkeiten wie das digitale Menschenmodell oder eine K�rperumrissschablone erm�glichen optimale Bedingungen f�r die Forschung.",
        			4
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.TRUE_FALSE.toString(), 
    				"Unter dem Begriff der virtuellen Ergonomie sind Software-Werkzeuge zusammenfassbar, mit denen ergonomische rechtswissenschaftliche Ziele verfolgt werden k�nnen.", 
        			"Richtig, Falsch", 
        			"Falsch",
        			"Unter dem Begriff sind Software-Werkzeuge zusammenfassbar, mit denen ergonomische arbeitswissenschaftliche Ziele verfolgt werden " +
        			"k�nnen. Dieses Themengebiet dreht sich um arbeitswissenschaftliche Untersuchungen, die als virtuelle Ergonomie bezeichnet werden.",
        			4
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.TRUE_FALSE.toString(), 
    				"In der Decke des Projekthauses sind ein Beamer und eine Leinwand integriert.", 
        			"Richtig, Falsch", 
        			"Falsch",
        			"Das Projekthaus ist mit modernster Technik ausgestattet und so sind in der Decke ein Beamer und eine Leinwand integriert. " +
        			"Diese bieten vor allem bei gro�en Konferenzen viele Vorteile.",
        			5
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Welchen Zweck erf�llen die Akustiklamellen, die �ber zwei Etagen installiert wurden?", 
        			"Schall reflektieren, Schall umlenken, Schall lauter machen", 
        			"Schall reflektieren",
        			"Die Akustiklamellen, die �ber zwei Etagen installiert wurden, sind ein besonderes Element im Projekthaus. " +
        			"Sie sollen den Schall reflektieren und abschirmen oder weiterleiten.",
        			5
        			);
    		questionDataSource.insertQuestion(
    				QuestionType.MULTIPLE_ANSWER.toString(), 
    				"Welche Aussagen �ber die Rundung sind korrekt?", 
        			"Aus dem Ausland, Besteht aus 6 Teilen, Von FiberTech gefertigt, aus glasfaserverst�rktem Kunststoff, Vor Ort zusammengebaut", 
        			"Besteht aus 6 Teilen, aus glasfaserverst�rktem Kunststoff",
        			"Die Rundung ist aus glasfaserverst�rktem Kunststoff und besteht aus 5 Teilen, die bei FiberTech in Chemnitz " +
        			"gefertigt und vor Ort zusammengebaut wurden. In dieser Gr��e und als freitragende Bauteile sind die Elemente einzigartig.",
        			5
        			);
    		
    		/*questionDataSource.insertQuestion(
    				QuestionType.FILL_IN_THE_BLANKS.toString(), 
    				"Das flexible Raumkonzept des METEORs erlaubt den Mitarbeitern eine individuelle " +
    				"Raumgestaltung. Ein Umbau der Ausstattung erfolgt etwa aller ____ Wochen. Dank dem Konzept " +
    				"sind insgesamt ____ verschiedene Raumbildungen m�glich. ", 
        			null, "2, 10",""
        			);*/
    	}
    	questionDataSource.close();
    }
    
    private void createAndInsertPageContents(){
    	pageContentDataSource = new PageContentDataSource(this);
    	pageContentDataSource.open();
    	pageContentDataSource.getDbHelper().dropTable(pageContentDataSource.getDatabase(), PageContentSQLiteHelper.TABLE_PAGE_CONTENT);
    	pageContentDataSource.getDbHelper().createTable(pageContentDataSource.getDatabase(), PageContentSQLiteHelper.TABLE_PAGE_CONTENT);
    	boolean isEmpty = pageContentDataSource.isTableEmpty(PageContentSQLiteHelper.TABLE_PAGE_CONTENT);
    	if(isEmpty){
    		pageContentDataSource.insertPageContent(1, R.string.title_area_1_page_1, R.drawable.image_area_1_page_1, R.string.text_area_1_page_1, 
    				null, null, null, null, 1, 0);
    		pageContentDataSource.insertPageContent(2, R.string.title_area_1_page_2, R.drawable.image_area_1_page_2, R.string.text_area_1_page_2, 
    				null, null, null, null, 1, 0);
    		/*pageContentDataSource.insertPageContent(3, R.string.title_page_3, R.drawable.image_page_3, R.string.text_page_3, 
    				String.valueOf(R.string.title_popup_3), null, String.valueOf(R.string.text_popup_3), 
    				String.valueOf(R.raw.playing_with_nao), 1);*/
    		pageContentDataSource.insertPageContent(3, R.string.title_area_1_page_3, R.drawable.image_area_1_page_3, R.string.text_area_1_page_3, 
    				String.valueOf(R.string.title_popup_3), String.valueOf(R.drawable.image_popup_area_1_page_3), 
    				String.valueOf(R.string.text_popup_area_1_page_3), null, 1, 1);
    		pageContentDataSource.insertPageContent(4, R.string.title_area_1_page_4, R.drawable.image_area_1_page_4, R.string.text_area_1_page_4, 
    				String.valueOf(R.string.title_popup_4), String.valueOf(R.drawable.image_popup_area_1_page_4), 
    				String.valueOf(R.string.text_popup_area_1_page_4), null, 1, 1);
    		pageContentDataSource.insertPageContent(5, R.string.title_area_1_page_5, R.drawable.image_area_1_page_5, R.string.text_area_1_page_5, 
    				String.valueOf(R.string.title_popup_5), 
    				String.valueOf(R.drawable.image1_popup_area_1_page_5 + "," + R.drawable.image2_popup_area_1_page_5 + "," +
    						R.drawable.image3_popup_area_1_page_5), 
    				String.valueOf(R.string.text_popup_area_1_page_5), null, 1, 1);
   
    		pageContentDataSource.insertPageContent(6, R.string.title_area_2_page_1, R.drawable.image_area_2_page_1, R.string.text_area_2_page_1, 
    				null, null, null, null, 2, 0);
    		pageContentDataSource.insertPageContent(7, R.string.title_area_2_page_2, R.drawable.image_area_2_page_2, R.string.text_area_2_page_2, 
    				String.valueOf(R.string.title_popup_3), String.valueOf(R.drawable.image_popup_area_2_page_2), 
    				String.valueOf(R.string.text_popup_area_2_page_2), null, 2, 1);
    		/*pageContentDataSource.insertPageContent(3, R.string.title_page_3, R.drawable.image_page_3, R.string.text_page_3, 
    				String.valueOf(R.string.title_popup_3), null, String.valueOf(R.string.text_popup_3), 
    				String.valueOf(R.raw.playing_with_nao), 1);*/
    		pageContentDataSource.insertPageContent(8, R.string.title_area_2_page_3, R.drawable.image_area_2_page_3, R.string.text_area_2_page_3, 
    				String.valueOf(R.string.title_popup_3), null, 
    				String.valueOf(R.string.text_popup_area_2_page_3), null, 2, 1);
    		pageContentDataSource.insertPageContent(9, R.string.title_area_2_page_4, R.drawable.image_area_2_page_4, R.string.text_area_2_page_4, 
    				null, null, null, null, 2, 0);
    		pageContentDataSource.insertPageContent(10, R.string.title_area_2_page_5, R.drawable.image_area_2_page_5, R.string.text_area_2_page_5, 
    				String.valueOf(R.string.title_popup_5), null, 
    				String.valueOf(R.string.text_popup_area_2_page_5), null, 2, 1);
    		
    		pageContentDataSource.insertPageContent(11, R.string.title_area_3_page_1, R.drawable.image_area_3_page_1, R.string.text_area_3_page_1, 
    				null, null, null, null, 3, 0);
    		pageContentDataSource.insertPageContent(12, R.string.title_area_3_page_2, R.drawable.image_empty_image, R.string.text_area_3_page_2, 
    				null, String.valueOf(R.drawable.image_popup_area_3_page_2), String.valueOf(R.string.text_popup_area_3_page_2), 
    				null, 3, 1);
    		pageContentDataSource.insertPageContent(13, R.string.title_area_3_page_3, R.drawable.image_area_3_page_3, R.string.text_area_3_page_3, 
    				null, null, String.valueOf(R.string.text_popup_area_3_page_3), 
    				null, 3, 1);
    		pageContentDataSource.insertPageContent(14, R.string.title_area_3_page_4, R.drawable.image_area_3_page_4, R.string.text_area_3_page_4, 
    				null, null, String.valueOf(R.string.text_popup_area_3_page_4), 
    				null, 3, 1);
    		
    		pageContentDataSource.insertPageContent(15, R.string.title_area_4_page_1, R.drawable.image_empty_image, R.string.text_area_4_page_1, 
    				null, null, null, null, 4, 0);
    		pageContentDataSource.insertPageContent(16, R.string.title_area_4_page_2, R.drawable.image_empty_image, R.string.text_area_4_page_2, 
    				null, null, null, null, 4, 0);
    		pageContentDataSource.insertPageContent(17, R.string.title_area_4_page_3, R.drawable.image_empty_image, R.string.text_area_4_page_3, 
    				null, null, null, null, 4, 0);
    		pageContentDataSource.insertPageContent(18, R.string.title_area_4_page_4, R.drawable.image_empty_image, R.string.text_area_4_page_4, 
    				null, null, null, null, 4, 0);
    		
    		pageContentDataSource.insertPageContent(19, R.string.title_area_5_page_1, R.drawable.image_empty_image, R.string.text_area_5_page_1, 
    				null, null, null, null, 5, 0);
    		pageContentDataSource.insertPageContent(20, R.string.title_area_5_page_2, R.drawable.image_empty_image, R.string.text_area_5_page_2, 
    				null, null, null, null, 5, 0);
    		pageContentDataSource.insertPageContent(21, R.string.title_area_5_page_3, R.drawable.image_empty_image, R.string.text_area_5_page_3, 
    				null, null, null, null, 5, 0);
    		pageContentDataSource.insertPageContent(22, R.string.title_area_5_page_4, R.drawable.image_empty_image, R.string.text_area_5_page_4, 
    				null, null, null, null, 5, 0);
    		
    	}
    	pageContentDataSource.close();
    }
    
    private void createAndInsertDirectionVideos(){
    	directionVideoDataSource = new DirectionVideoDataSource(this);
    	directionVideoDataSource.open();
    	directionVideoDataSource.getDbHelper().dropTable(directionVideoDataSource.getDatabase(), DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO);
    	directionVideoDataSource.getDbHelper().createTable(directionVideoDataSource.getDatabase(), DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO);
    	boolean isEmpty = directionVideoDataSource.isTableEmpty(DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO);
    	if(isEmpty){
    		directionVideoDataSource.insertDirectionVideo(1, 2, String.valueOf(R.raw.point_0_to_1));
    		directionVideoDataSource.insertDirectionVideo(2, 3, String.valueOf(R.raw.point_1_to_2));
    		directionVideoDataSource.insertDirectionVideo(3, 4, String.valueOf(R.raw.point_2_to_3));
    		directionVideoDataSource.insertDirectionVideo(4, 5, String.valueOf(R.raw.point_3_to_4));
    		
    		
    	}
    	directionVideoDataSource.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
