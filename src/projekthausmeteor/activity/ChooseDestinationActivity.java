package projekthausmeteor.activity;

import java.util.List;

import com.example.projekthausmeteor.R;

import projekthausmeteor.adapter.LaboratoryListAdapter;
import projekthausmeteor.data.dao.PageContentDataSource;
import projekthausmeteor.data.model.PageContent;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ChooseDestinationActivity extends Activity {
	
	private PageContentDataSource dataSource;
	public static List<PageContent> allPageContents;
	private ListView listview;
	private Context context;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		dataSource = new PageContentDataSource(this);
		dataSource.open();
		ChooseDestinationActivity.allPageContents = dataSource.getAllPageContents();
		dataSource.close();
		
		LaboratoryListAdapter adapter = new LaboratoryListAdapter(this);
	    //setListAdapter(adapter);
	    setContentView(R.layout.list_layout);
	    listview = (ListView)findViewById(R.id.list);
	    listview.setAdapter(adapter);
	    
	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	        @Override
	        public void onItemClick(AdapterView<?> parent, final View view,
	            int position, long id) {
	        	TextView itemId = ((TextView)view.findViewById(R.id.textView_item_id));
	    		int pageId = Integer.parseInt(itemId.getText().toString());
	    		Intent pageIntent = new Intent(context, PageActivity.class);
	    		pageIntent.putExtra("pageId", pageId);
	    		context.startActivity(pageIntent);
	        }

	      });
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig){

    	if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
    		MainActivity.currentOrientation = Configuration.ORIENTATION_LANDSCAPE;
        }
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
        	MainActivity.currentOrientation = Configuration.ORIENTATION_PORTRAIT;
        }
    	
    	super.onConfigurationChanged(newConfig);

        
    }

	/*@Override
	protected void onListItemClick(ListView l, View view, int position, long id) {
		
		TextView itemId = ((TextView)view.findViewById(R.id.textView_item_id));
		int pageId = Integer.parseInt(itemId.getText().toString());
		Intent pageIntent = new Intent(this, PageViewActivity.class);
		pageIntent.putExtra("pageId", pageId);
		this.startActivity(pageIntent);
		  
	  }*/
} 