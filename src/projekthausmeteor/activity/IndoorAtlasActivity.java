package projekthausmeteor.activity;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.projekthausmeteor.R;
import com.indooratlas.android.CalibrationState;
import com.indooratlas.android.FloorPlan;
import com.indooratlas.android.FutureResult;
import com.indooratlas.android.IndoorAtlas;
import com.indooratlas.android.IndoorAtlasException;
import com.indooratlas.android.IndoorAtlasFactory;
import com.indooratlas.android.IndoorAtlasListener;
import com.indooratlas.android.ServiceState;

/**
 * <p>Activity to demonstrate basic use of IndoorAtlas SDK. If there are no public maps around your
 * location, you can create a map of your own and upload it to IndoorAtlas servers. Read more on how
 * to do this from: http://developer.indooratlas.com.</p>
 * <p/>
 * <p>To run this demo, you will also need your applications API key/secret and identifiers for the
 * floor plan from http://developer.indooratlas.com.</p>
 * <p/>
 */
public class IndoorAtlasActivity extends Activity implements IndoorAtlasListener {

    private static final String TAG = "IndoorAtlasActivity";

    private ListView mLogView;
    private ImageView mMapImage;
    private LogAdapter mLogAdapter;
    private Bitmap bitmapImageMap;

    private IndoorAtlas mIndoorAtlas;
    private boolean mIsPositioning;
    private StringBuilder mSharedBuilder = new StringBuilder();

    private String mApiKey = "ninejulkar";
    private String mApiSecret = "junnesatawaz2016";

    private String mVenueId = "44179368-3087-4044-8507-1a8a6e1730a1";
    private String mFloorId = "7cc2d643-c481-416f-8c3b-34e089777ee8";
    private String mFloorPlanId = "b8d1e3e8-90d1-464b-b399-4abbd6de9ef6";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.indoor_atlas);

        mLogView = (ListView) findViewById(R.id.list);
        mLogAdapter = new LogAdapter(this);
        mLogView.setAdapter(mLogAdapter);

        mMapImage = (ImageView) findViewById(R.id.imageView_map);
        initIndoorAtlas();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tearDown();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_indoor_atlas, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear_log:
                mLogAdapter.clear();
                return true;
            case R.id.action_toggle_positioning:
                togglePositioning();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void tearDown() {
        if (mIndoorAtlas != null) {
            mIndoorAtlas.tearDown();
        }
    }


    private void stopPositioning() {
        mIsPositioning = false;
        if (mIndoorAtlas != null) {
            log("Stop positioning");
            mIndoorAtlas.stopPositioning();
        }
    }

    private void startPositioning() {
        if (mIndoorAtlas != null && mIndoorAtlas.isCalibrationReady()) {
            log(String.format("startPositioning, venueId: %s, floorId: %s, floorPlanId: %s",
                    mVenueId,
                    mFloorId,
                    mFloorPlanId));
            try {
                mIndoorAtlas.startPositioning(mVenueId, mFloorId, mFloorPlanId);
                mIsPositioning = true;
            } catch (IndoorAtlasException e) {
                log("startPositioning failed: " + e);
            }
        } else {
            log("calibration not ready, cannot start positioning");
        }
    }

    private void togglePositioning() {
        if (mIsPositioning) {
            stopPositioning();
        } else {
            startPositioning();
        }
    }

    private void initIndoorAtlas() {

        try {

            log("Connecting with IndoorAtlas, apiKey: " + mApiKey);

            // obtain instance to positioning service, note that calibrating might begin instantly
            mIndoorAtlas = IndoorAtlasFactory.createIndoorAtlas(
                    getApplicationContext(),
                    this, // IndoorAtlasListener
                    mApiKey,
                    mApiSecret);
            
            log("IndoorAtlas instance created");
            Thread fetchMapImageThread = new Thread(new Runnable(){

				@Override
				public void run() {
					try{
						FutureResult<FloorPlan> futureResultFloorPlan = mIndoorAtlas.fetchFloorPlan(mFloorPlanId);
			            FloorPlan floorPlan = futureResultFloorPlan.get();
			            
			            FutureResult<Bitmap> futureResultFloorPlanImage = mIndoorAtlas.fetchFloorPlanImage(floorPlan, null);
			            if(futureResultFloorPlanImage != null){
			            	bitmapImageMap = futureResultFloorPlanImage.get();
			            	
			            }
			            else{
			            	bitmapImageMap = BitmapFactory.decodeResource(getResources(), R.drawable.map_not_found);
			            }
					}catch (IndoorAtlasException ex){
						Log.e("IndoorAtlas", "init failed", ex);
			            log("init IndoorAtlas failed, " + ex.toString());
					}catch (IOException ex){
			        	Log.e("IndoorAtlas", "init failed", ex);
			            log("init IndoorAtlas failed, " + ex.toString());
					}
					
				}
            	
            });
            fetchMapImageThread.start();
            mMapImage.setImageBitmap(bitmapImageMap);

        } catch (IndoorAtlasException ex) {
            Log.e("IndoorAtlas", "init failed", ex);
            log("init IndoorAtlas failed, " + ex.toString());
        }

    }


    private void log(final String msg) {
        Log.d(TAG, msg);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLogAdapter.add(msg);
                mLogAdapter.notifyDataSetChanged();
            }
        });
    }

    /* IndoorAtlasListener interface */

    /**
     * This is where you will handle location updates.
     */
    public void onServiceUpdate(ServiceState state) {

        mSharedBuilder.setLength(0);
        mSharedBuilder.append("Location: ")
                .append("\n\troundtrip : ").append(state.getRoundtrip()).append("ms")
                .append("\n\tlat : ").append(state.getGeoPoint().getLatitude())
                .append("\n\tlon : ").append(state.getGeoPoint().getLongitude())
                .append("\n\tX [meter] : ").append(state.getMetricPoint().getX())
                .append("\n\tY [meter] : ").append(state.getMetricPoint().getY())
                .append("\n\tI [pixel] : ").append(state.getImagePoint().getI())
                .append("\n\tJ [pixel] : ").append(state.getImagePoint().getJ())
                .append("\n\theading : ").append(state.getHeadingDegrees())
                .append("\n\tuncertainty: ").append(state.getUncertainty());
        
        log(mSharedBuilder.toString());
    }


    @Override
    public void onServiceFailure(int errorCode, String reason) {
        log("onServiceFailure: reason : " + reason);
    }

    @Override
    public void onServiceInitializing() {
        log("onServiceInitializing()");
    }

    @Override
    public void onServiceInitialized() {
        log("onServiceInitialized()");
    }

    @Override
    public void onInitializationFailed(final String reason) {
        log("onInitializationFailed(): " + reason);
    }

    @Override
    public void onServiceStopped() {
        log("onServiceStopped()");
    }

    @Override
    public void onCalibrationStatus(CalibrationState calibrationState) {

        log("onCalibrationStatus: event: " + calibrationState.getCalibrationEvent()
                + ", percentage: " + calibrationState.getPercentage());

    }

    @Override
    public void onCalibrationFailed(String reason) {
        log("onCalibrationFailed(): Please do figure '8' motion until " +
                "onCalibrationFinished() or onCalibrationFailed() is called");
    }

    @Override
    public void onCalibrationInvalid() {
        log("Calibration Invalid");
    }

    /**
     * Calibration successful, positioning can be started
     */
    @Override
    public void onCalibrationReady() {
        log("onCalibrationReady");
        startPositioning();
    }

    @Override
    public void onNetworkChangeComplete(boolean success) {
    }


    static class LogAdapter extends BaseAdapter {

        private ArrayList<String> mLines = new ArrayList<String>();
        private LayoutInflater mInflater;

        public LogAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mLines.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView text = (TextView) convertView;
            if (convertView == null) {
                text = (TextView) mInflater.inflate(android.R.layout.simple_list_item_1, parent,
                        false);
            }
            text.setText(mLines.get(position));
            return text;
        }

        public void add(String line) {
            mLines.add(0, line);
        }

        public void clear() {
            mLines.clear();
            notifyDataSetChanged();
        }
    }

}
