package projekthausmeteor.adapter;

import java.util.List;

import projekthausmeteor.activity.ChooseDestinationActivity;
import projekthausmeteor.data.dao.PageContentDataSource;
import projekthausmeteor.data.model.PageContent;

import com.example.projekthausmeteor.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LaboratoryListAdapter extends BaseAdapter{
	
	private final Activity context;
	private LinearLayout listItem;
	private ImageView itemImage;
	private TextView itemText, itemId;
	
	
	public LaboratoryListAdapter(Activity context) {
		super();
		this.context = context;
		
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		
		if(view == null){
			view = (LinearLayout)context.getLayoutInflater().inflate(R.layout.list_item, 
					parent, false);
		}
		/*listItem = (LinearLayout)context.getLayoutInflater().inflate(R.layout.list_item, 
				new LinearLayout(context), false);*/
		//listItem = (LinearLayout)view;
		itemImage = (ImageView)view.findViewById(R.id.imageView_item_image);
		itemText = (TextView)view.findViewById(R.id.textView_item_text);
		itemId = (TextView)view.findViewById(R.id.textView_item_id);
		
		PageContent pageContent = ChooseDestinationActivity.allPageContents.get(position);
		itemImage.setImageResource(pageContent.getPageImageId());
		itemText.setText(pageContent.getPageTitleId());
		itemId.setText("" + pageContent.getPageId());
		
		return view;
	}

	@Override
	public int getCount() {
		return ChooseDestinationActivity.allPageContents.size();
	}

	@Override
	public Object getItem(int position) {
		return ChooseDestinationActivity.allPageContents.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
