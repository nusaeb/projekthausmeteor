package projekthausmeteor.data.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DirectionVideoSQLiteHelper extends SQLiteOpenHelper{

	public static final String TABLE_DIRECTION_VIDEO = "direction_video";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_FROM_ROOM_ID = "from_room_id";
	public static final String COLUMN_TO_ROOM_ID = "to_room_id";
	public static final String COLUMN_VIDEO_PATH = "video_path";
	
	private static final String DATABASE_NAME = "directionvideo.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String CREATE_TABLE_DIRECTION_VIDEO = "create table " + TABLE_DIRECTION_VIDEO + "(" 
			+ COLUMN_ID + " integer primary key autoincrement, " 
			+ COLUMN_FROM_ROOM_ID + " integer not null, "
			+ COLUMN_TO_ROOM_ID + " integer not null, "
			+ COLUMN_VIDEO_PATH + " text"
			+ ");";
	
	private static final String CREATE_DATABASE = CREATE_TABLE_DIRECTION_VIDEO;
	
	public DirectionVideoSQLiteHelper(Context context) {
		// Context context, String name, CursorFactory factory, int version
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_DATABASE);		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DirectionVideoSQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIRECTION_VIDEO);
	    onCreate(db);
	}
	
	public void createTable(SQLiteDatabase db, String tableName) throws android.database.SQLException{
		if(tableName.equalsIgnoreCase(DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO)){
			db.execSQL(DirectionVideoSQLiteHelper.CREATE_TABLE_DIRECTION_VIDEO);
			Log.i("Success", tableName+ " is created successfully");
		}
		
	}
	
	public void dropTable(SQLiteDatabase db, String tableName) throws android.database.SQLException{
		db.execSQL("DROP TABLE IF EXISTS " + tableName);
		Log.i("Success", tableName+ " is dropped successfully");
	}

}
