package projekthausmeteor.data.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class QuestionSQLiteHelper extends SQLiteOpenHelper{

	public static final String TABLE_QUESTION = "question";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_QUESTION_TYPE = "question_type";
	public static final String COLUMN_QUESTION = "question";
	public static final String COLUMN_POSSIBLE_ANSWERS = "possible_answers";
	public static final String COLUMN_CORRECT_ANSWERS = "correct_answers";
	public static final String COLUMN_COMMENTS = "comment";
	public static final String COLUMN_QUESTION_FOR_AREA_ID = "question_for_area_id";
	
	private static final String DATABASE_NAME = "question.db";
	private static final int DATABASE_VERSION = 2;
	
	private static final String CREATE_TABLE_QUESTION = "create table " + TABLE_QUESTION + "(" 
			+ COLUMN_ID + " integer primary key autoincrement, " 
			+ COLUMN_QUESTION_TYPE + " text not null, "
			+ COLUMN_QUESTION + " text not null, "
			+ COLUMN_POSSIBLE_ANSWERS + " text, "
			+ COLUMN_CORRECT_ANSWERS + " text,"
			+ COLUMN_COMMENTS + " text,"
			+ COLUMN_QUESTION_FOR_AREA_ID + " integer not null"
			+ ");";
	
	private static final String CREATE_DATABASE = CREATE_TABLE_QUESTION;
	
	public QuestionSQLiteHelper(Context context) {
		// Context context, String name, CursorFactory factory, int version
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_DATABASE);		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(QuestionSQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTION);
	    onCreate(db);
	}
	
	public void createTable(SQLiteDatabase db, String tableName) throws android.database.SQLException{
		if(tableName.equalsIgnoreCase(QuestionSQLiteHelper.TABLE_QUESTION)){
			db.execSQL(QuestionSQLiteHelper.CREATE_TABLE_QUESTION);
			Log.i("Success", tableName+ " is created successfully");
		}
		
	}
	
	public void dropTable(SQLiteDatabase db, String tableName) throws android.database.SQLException{
		db.execSQL("DROP TABLE IF EXISTS " + tableName);
		Log.i("Success", tableName+ " is dropped successfully");
	}

}
