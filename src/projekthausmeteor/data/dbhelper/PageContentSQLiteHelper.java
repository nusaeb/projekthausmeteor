package projekthausmeteor.data.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PageContentSQLiteHelper extends SQLiteOpenHelper{

	public static final String TABLE_PAGE_CONTENT = "page_content";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_PAGE_ID = "page_id";
	public static final String COLUMN_PAGE_TITLE_ID = "page_title_id";
	public static final String COLUMN_PAGE_IMAGE_ID = "page_image_id";
	public static final String COLUMN_PAGE_TEXT_ID = "page_text_id";
	public static final String COLUMN_PAGE_POPUP_TITLE_ID = "page_popup_title_id";
	public static final String COLUMN_PAGE_POPUP_IMAGE_ID = "page_popup_image_id";
	public static final String COLUMN_PAGE_POPUP_TEXT_ID = "page_popup_text_id";
	public static final String COLUMN_PAGE_POPUP_MULTIMEDIA_ID = "page_popup_multimedia_id";
	public static final String COLUMN_AREA_ID = "area_id";
	public static final String COLUMN_HAS_POPUP = "has_popup";
	
	private static final String DATABASE_NAME = "pagecontent.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String CREATE_TABLE_PAGE_CONTENT = "create table " + TABLE_PAGE_CONTENT + "(" 
			+ COLUMN_ID + " integer primary key autoincrement, " 
			+ COLUMN_PAGE_ID + " integer not null, "
			+ COLUMN_PAGE_TITLE_ID + " integer not null, "
			+ COLUMN_PAGE_IMAGE_ID + " integer not null, "
			+ COLUMN_PAGE_TEXT_ID + " integer not null, "
			+ COLUMN_PAGE_POPUP_TITLE_ID + " text, "
			+ COLUMN_PAGE_POPUP_IMAGE_ID + " text, "
			+ COLUMN_PAGE_POPUP_TEXT_ID + " text, "
			+ COLUMN_PAGE_POPUP_MULTIMEDIA_ID + " text, "
			+ COLUMN_AREA_ID + " integer not null, "
			+ COLUMN_HAS_POPUP + " integer not null"
			+ ");";
	
	private static final String CREATE_DATABASE = CREATE_TABLE_PAGE_CONTENT;
	
	public PageContentSQLiteHelper(Context context) {
		// Context context, String name, CursorFactory factory, int version
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_DATABASE);		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(PageContentSQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAGE_CONTENT);
	    onCreate(db);
	}
	
	public void createTable(SQLiteDatabase db, String tableName) throws android.database.SQLException{
		if(tableName.equalsIgnoreCase(PageContentSQLiteHelper.TABLE_PAGE_CONTENT)){
			db.execSQL(PageContentSQLiteHelper.CREATE_TABLE_PAGE_CONTENT);
			Log.i("Success", tableName+ " is created successfully");
		}
		
	}
	
	public void dropTable(SQLiteDatabase db, String tableName) throws android.database.SQLException{
		db.execSQL("DROP TABLE IF EXISTS " + tableName);
		Log.i("Success", tableName+ " is dropped successfully");
	}

}
