package projekthausmeteor.data.dao;

import java.util.ArrayList;
import java.util.List;

import projekthausmeteor.data.dbhelper.PageContentSQLiteHelper;
import projekthausmeteor.data.model.PageContent;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class PageContentDataSource {

	//Database fields
	private SQLiteDatabase database;
	private PageContentSQLiteHelper dbHelper;
	private String[] allColumns = { PageContentSQLiteHelper.COLUMN_ID,
			PageContentSQLiteHelper.COLUMN_PAGE_ID,
			PageContentSQLiteHelper.COLUMN_PAGE_TITLE_ID,
			PageContentSQLiteHelper.COLUMN_PAGE_IMAGE_ID,
			PageContentSQLiteHelper.COLUMN_PAGE_TEXT_ID, 
			PageContentSQLiteHelper.COLUMN_PAGE_POPUP_TITLE_ID,
			PageContentSQLiteHelper.COLUMN_PAGE_POPUP_IMAGE_ID,
			PageContentSQLiteHelper.COLUMN_PAGE_POPUP_TEXT_ID,
			PageContentSQLiteHelper.COLUMN_PAGE_POPUP_MULTIMEDIA_ID,
			PageContentSQLiteHelper.COLUMN_AREA_ID,
			PageContentSQLiteHelper.COLUMN_HAS_POPUP };

	public PageContentDataSource(Context context) {
		dbHelper = new PageContentSQLiteHelper(context);
	}

	public void open() throws android.database.SQLException {
	    database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public List<PageContent> getAllPageContents() {
		List<PageContent> pageContents = new ArrayList<PageContent>();
		
		Cursor cursor = database.query(PageContentSQLiteHelper.TABLE_PAGE_CONTENT,
				allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			PageContent pageContent = cursorToPageContent(cursor);
			pageContents.add(pageContent);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return pageContents;
	}
	
	public List<PageContent> getAllPageContentsByAreaId(int areaId) {
		List<PageContent> pageContents = new ArrayList<PageContent>();
		
		Cursor cursor = database.query(PageContentSQLiteHelper.TABLE_PAGE_CONTENT,
		        allColumns, PageContentSQLiteHelper.COLUMN_AREA_ID + " = " + areaId, null,
		        null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			PageContent pageContent = cursorToPageContent(cursor);
			pageContents.add(pageContent);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return pageContents;
	}
		
	public PageContent getPageContentById(long id){
		Cursor cursor = database.query(PageContentSQLiteHelper.TABLE_PAGE_CONTENT,
        allColumns, PageContentSQLiteHelper.COLUMN_ID + " = " + id, null,
        null, null, null);
	    cursor.moveToFirst();
	    PageContent pageContent = cursorToPageContent(cursor);
	    cursor.close();
	    return pageContent;
	}
	
	public PageContent getPageContentByPageId(int pageId){
		Cursor cursor = database.query(PageContentSQLiteHelper.TABLE_PAGE_CONTENT,
        allColumns, PageContentSQLiteHelper.COLUMN_PAGE_ID + " = " + pageId, null,
        null, null, null);
	    cursor.moveToFirst();
	    PageContent pageContent = cursorToPageContent(cursor);
	    cursor.close();
	    return pageContent;
	}
	public long insertPageContent(int pageId, int pageTitleId, int pageImageId, int pageTextId,
			String pagePopupTitleId, String pagePopupImageId, String pagePopupTextId,
			String pagePopupMultimediaId, int areaId, int hasPopup) {
		ContentValues values = new ContentValues();
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_ID, pageId);
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_TITLE_ID, pageTitleId);
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_IMAGE_ID, pageImageId);
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_TEXT_ID, pageTextId);
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_POPUP_TITLE_ID, pagePopupTitleId);
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_POPUP_IMAGE_ID, pagePopupImageId);
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_POPUP_TEXT_ID, pagePopupTextId);
		values.put(PageContentSQLiteHelper.COLUMN_PAGE_POPUP_MULTIMEDIA_ID, pagePopupMultimediaId);
		values.put(PageContentSQLiteHelper.COLUMN_AREA_ID, areaId);
		values.put(PageContentSQLiteHelper.COLUMN_HAS_POPUP, hasPopup);
		
		long insertId = database.insert(PageContentSQLiteHelper.TABLE_PAGE_CONTENT, null,
		    values);
	    return insertId;
	}

	public void deletePageContent(PageContent pageContent) {
	    long id = pageContent.getId();
	    System.out.println("PageContent deleted with id: " + id);
	    database.delete(PageContentSQLiteHelper.TABLE_PAGE_CONTENT, PageContentSQLiteHelper.COLUMN_ID
	        + " = " + id, null);
	}

	private PageContent cursorToPageContent(Cursor cursor) {
	    PageContent pageContent = new PageContent();
	    pageContent.setId(cursor.getLong(0));
	    pageContent.setPageId(cursor.getInt(1));
	    pageContent.setPageTitleId(cursor.getInt(2));
	    pageContent.setPageImageId(cursor.getInt(3));
	    pageContent.setPageTextId(cursor.getInt(4));
	    pageContent.setAreaId(cursor.getInt(9));
	    pageContent.setHasPopup(cursor.getInt(10));
	    
	    String popupTitleId = cursor.getString(5);
	    String popupImageId = cursor.getString(6);
	    String popupTextId = cursor.getString(7);
	    String popupMultimediaId = cursor.getString(8);
	    if(popupTitleId != null){
	    	String[] stringIds = popupTitleId.split(",");
	    	int[] intIds = new int[stringIds.length];
	    	for(int i=0; i<stringIds.length; i++){
	    		intIds[i] = Integer.parseInt(stringIds[i]);
	    	}
	    	pageContent.setPagePopupTitleId(intIds);
	    }
	    if(popupImageId != null){
	    	String[] stringIds = popupImageId.split(",");
	    	int[] intIds = new int[stringIds.length];
	    	for(int i=0; i<stringIds.length; i++){
	    		intIds[i] = Integer.parseInt(stringIds[i]);
	    	}
	    	pageContent.setPagePopupImageId(intIds);
	    }
	    if(popupTextId != null){
	    	String[] stringIds = popupTextId.split(",");
	    	int[] intIds = new int[stringIds.length];
	    	for(int i=0; i<stringIds.length; i++){
	    		intIds[i] = Integer.parseInt(stringIds[i]);
	    	}
	    	pageContent.setPagePopupTextId(intIds);
	    }
	    if(popupMultimediaId != null){
	    	String[] stringIds = popupMultimediaId.split(",");
	    	int[] intIds = new int[stringIds.length];
	    	for(int i=0; i<stringIds.length; i++){
	    		intIds[i] = Integer.parseInt(stringIds[i]);
	    	}
	    	pageContent.setPagePopupMultimediaId(intIds);
	    }
	    return pageContent;
	}
	
	public boolean isTableEmpty(String tableName){
		//open();
	    //Cursor cursor = database.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
		Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM " + tableName + ";", null);
		
		if(cursor != null) {
			cursor.moveToFirst();
	    	if(cursor.getInt(0) > 0) {
	    		cursor.close();
	            return false;
	        }
	    }
		return true;
	}

	public PageContentSQLiteHelper getDbHelper() {
		return dbHelper;
	}

	public void setDbHelper(PageContentSQLiteHelper dbHelper) {
		this.dbHelper = dbHelper;
	}
	
	public SQLiteDatabase getDatabase() {
		return database;
	}

	public void setDatabase(SQLiteDatabase database) {
		this.database = database;
	}
}
