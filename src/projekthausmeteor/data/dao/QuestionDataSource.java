package projekthausmeteor.data.dao;

import java.util.ArrayList;
import java.util.List;

import projekthausmeteor.data.dbhelper.QuestionSQLiteHelper;
import projekthausmeteor.data.model.Question;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class QuestionDataSource {

	//Database fields
	private SQLiteDatabase database;
	private QuestionSQLiteHelper dbHelper;
	private String[] allColumns = { QuestionSQLiteHelper.COLUMN_ID,
			QuestionSQLiteHelper.COLUMN_QUESTION_TYPE,
			QuestionSQLiteHelper.COLUMN_QUESTION,
			QuestionSQLiteHelper.COLUMN_POSSIBLE_ANSWERS,
			QuestionSQLiteHelper.COLUMN_CORRECT_ANSWERS,
			QuestionSQLiteHelper.COLUMN_COMMENTS,
			QuestionSQLiteHelper.COLUMN_QUESTION_FOR_AREA_ID};

	public QuestionDataSource(Context context) {
		dbHelper = new QuestionSQLiteHelper(context);
	}

	public void open() throws android.database.SQLException {
	    database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public List<Question> getAllQuestions() {
		List<Question> questions = new ArrayList<Question>();
		
		Cursor cursor = database.query(QuestionSQLiteHelper.TABLE_QUESTION,
				allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Question question = cursorToQuestion(cursor);
			questions.add(question);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return questions;
	}
	
	public List<Question> getAllQuestionsByAreaId(int questionForAreaId) {
		List<Question> questions = new ArrayList<Question>();
		
		Cursor cursor = database.query(QuestionSQLiteHelper.TABLE_QUESTION,
		        allColumns, QuestionSQLiteHelper.COLUMN_QUESTION_FOR_AREA_ID + " = " + questionForAreaId, 
		        null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Question question = cursorToQuestion(cursor);
			questions.add(question);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return questions;
	}
	
	public Question getQuestionById(long id){
		Cursor cursor = database.query(QuestionSQLiteHelper.TABLE_QUESTION,
        allColumns, QuestionSQLiteHelper.COLUMN_ID + " = " + id, null,
        null, null, null);
	    cursor.moveToFirst();
	    Question question = cursorToQuestion(cursor);
	    cursor.close();
	    return question;
	}
	public long insertQuestion(String questionType, String question, 
			String possibleAnswers, String correctAnswers, String comment,
			int questionForAreaId) {
		ContentValues values = new ContentValues();
		values.put(QuestionSQLiteHelper.COLUMN_QUESTION_TYPE, questionType);
		values.put(QuestionSQLiteHelper.COLUMN_QUESTION, question);
		values.put(QuestionSQLiteHelper.COLUMN_POSSIBLE_ANSWERS, possibleAnswers);
		values.put(QuestionSQLiteHelper.COLUMN_CORRECT_ANSWERS, correctAnswers);
		values.put(QuestionSQLiteHelper.COLUMN_COMMENTS, comment);
		values.put(QuestionSQLiteHelper.COLUMN_QUESTION_FOR_AREA_ID, questionForAreaId);
		long insertId = database.insert(QuestionSQLiteHelper.TABLE_QUESTION, null,
		    values);
	    return insertId;
	}

	public void deleteQuestion(Question question) {
	    long id = question.getId();
	    System.out.println("Question deleted with id: " + id);
	    database.delete(QuestionSQLiteHelper.TABLE_QUESTION, QuestionSQLiteHelper.COLUMN_ID
	        + " = " + id, null);
	}

	private Question cursorToQuestion(Cursor cursor) {
	    Question question = new Question();
	    question.setId(cursor.getLong(0));
	    question.setQuestionType(cursor.getString(1));
	    question.setQuestion(cursor.getString(2));
	    question.setPossibleAnswers(cursor.getString(3));
	    question.setCorrectAnswers(cursor.getString(4));
	    question.setComment(cursor.getString(5));
	    question.setQuestionForAreaId(cursor.getInt(6));
	    return question;
	}
	
	public boolean isTableEmpty(String tableName){
		//open();
	    //Cursor cursor = database.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
		Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM " + tableName + ";", null);
		
		if(cursor != null) {
			cursor.moveToFirst();
	    	if(cursor.getInt(0) > 0) {
	    		cursor.close();
	            return false;
	        }
	    }
		return true;
	}

	public QuestionSQLiteHelper getDbHelper() {
		return dbHelper;
	}

	public void setDbHelper(QuestionSQLiteHelper dbHelper) {
		this.dbHelper = dbHelper;
	}
	
	public SQLiteDatabase getDatabase() {
		return database;
	}

	public void setDatabase(SQLiteDatabase database) {
		this.database = database;
	}
}
