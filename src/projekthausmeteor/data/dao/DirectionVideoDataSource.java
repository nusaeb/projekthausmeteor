package projekthausmeteor.data.dao;

import java.util.ArrayList;
import java.util.List;

import projekthausmeteor.data.dbhelper.DirectionVideoSQLiteHelper;
import projekthausmeteor.data.model.DirectionVideo;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class DirectionVideoDataSource {

	//Database fields
	private SQLiteDatabase database;
	private DirectionVideoSQLiteHelper dbHelper;
	private String[] allColumns = { DirectionVideoSQLiteHelper.COLUMN_ID,
			DirectionVideoSQLiteHelper.COLUMN_FROM_ROOM_ID,
			DirectionVideoSQLiteHelper.COLUMN_TO_ROOM_ID,
			DirectionVideoSQLiteHelper.COLUMN_VIDEO_PATH
			};

	public DirectionVideoDataSource(Context context) {
		dbHelper = new DirectionVideoSQLiteHelper(context);
	}

	public void open() throws android.database.SQLException {
	    database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public List<DirectionVideo> getAllDirectionVideos() {
		List<DirectionVideo> directionVideos = new ArrayList<DirectionVideo>();
		
		Cursor cursor = database.query(DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO,
				allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			DirectionVideo directionVideo = cursorToDirectionVideo(cursor);
			directionVideos.add(directionVideo);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return directionVideos;
	}
	
	public DirectionVideo getDirectionVideoById(long id){
		Cursor cursor = database.query(DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO,
        allColumns, DirectionVideoSQLiteHelper.COLUMN_ID + " = " + id, null,
        null, null, null);
	    cursor.moveToFirst();
	    DirectionVideo directionVideo = cursorToDirectionVideo(cursor);
	    cursor.close();
	    return directionVideo;
	}
	
	public DirectionVideo getDirectionVideoFrom2To(int fromRoomId, int toRoomId){
		Cursor cursor = database.query(DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO,
        allColumns, 
        DirectionVideoSQLiteHelper.COLUMN_FROM_ROOM_ID + " =? AND "
        + DirectionVideoSQLiteHelper.COLUMN_TO_ROOM_ID + " =? ", 
        new String[]{String.valueOf(fromRoomId), String.valueOf(toRoomId)}, null, null, null);
	    cursor.moveToFirst();
	    DirectionVideo directionVideo = cursorToDirectionVideo(cursor);
	    cursor.close();
	    return directionVideo;
	}
	public long insertDirectionVideo(int fromRoomId, int toRoomId, String videoPath) {
		ContentValues values = new ContentValues();
		values.put(DirectionVideoSQLiteHelper.COLUMN_FROM_ROOM_ID, fromRoomId);
		values.put(DirectionVideoSQLiteHelper.COLUMN_TO_ROOM_ID, toRoomId);
		values.put(DirectionVideoSQLiteHelper.COLUMN_VIDEO_PATH, videoPath);
				
		long insertId = database.insert(DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO, null,
		    values);
	    return insertId;
	}

	public void deleteDirectionVideo(DirectionVideo directionVideo) {
	    long id = directionVideo.getId();
	    System.out.println("DirectionVideo deleted with id: " + id);
	    database.delete(DirectionVideoSQLiteHelper.TABLE_DIRECTION_VIDEO, DirectionVideoSQLiteHelper.COLUMN_ID
	        + " = " + id, null);
	}

	private DirectionVideo cursorToDirectionVideo(Cursor cursor) {
		DirectionVideo directionVideo = new DirectionVideo();
		directionVideo.setId(cursor.getLong(0));
		directionVideo.setFromRoomId(cursor.getInt(1));
		directionVideo.setToRoomId(cursor.getInt(2));
		directionVideo.setVideoPath(cursor.getString(3));

	    return directionVideo;
	}
	
	public boolean isTableEmpty(String tableName){
		//open();
	    //Cursor cursor = database.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
		Cursor cursor = database.rawQuery("SELECT COUNT(*) FROM " + tableName + ";", null);
		
		if(cursor != null) {
			cursor.moveToFirst();
	    	if(cursor.getInt(0) > 0) {
	    		cursor.close();
	            return false;
	        }
	    }
		return true;
	}

	public DirectionVideoSQLiteHelper getDbHelper() {
		return dbHelper;
	}

	public void setDbHelper(DirectionVideoSQLiteHelper dbHelper) {
		this.dbHelper = dbHelper;
	}
	
	public SQLiteDatabase getDatabase() {
		return database;
	}

	public void setDatabase(SQLiteDatabase database) {
		this.database = database;
	}
}
