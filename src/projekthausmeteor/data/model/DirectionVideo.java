package projekthausmeteor.data.model;

public class DirectionVideo {
	
	private long id;
	private int fromRoomId;
	private int toRoomId;
	private String videoPath;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getFromRoomId() {
		return fromRoomId;
	}
	public void setFromRoomId(int fromRoomId) {
		this.fromRoomId = fromRoomId;
	}
	public int getToRoomId() {
		return toRoomId;
	}
	public void setToRoomId(int toRoomId) {
		this.toRoomId = toRoomId;
	}
	public String getVideoPath() {
		return videoPath;
	}
	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}
	
	

}
