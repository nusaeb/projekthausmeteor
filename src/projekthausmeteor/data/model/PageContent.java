package projekthausmeteor.data.model;

public class PageContent {
	
	private long id;
	private int pageId;
	private int pageTitleId;
	private int pageImageId;
	private int pageTextId;
	private int[] pagePopupTitleId;
	private int[] pagePopupImageId;
	private int[] pagePopupTextId;
	private int[] pagePopupMultimediaId;
	private int areaId;
	private int hasPopup;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getPageId() {
		return pageId;
	}
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	public int getPageTitleId() {
		return pageTitleId;
	}
	public void setPageTitleId(int pageTitleId) {
		this.pageTitleId = pageTitleId;
	}
	public int getPageImageId() {
		return pageImageId;
	}
	public void setPageImageId(int pageImageId) {
		this.pageImageId = pageImageId;
	}
	public int getPageTextId() {
		return pageTextId;
	}
	public void setPageTextId(int pageTextId) {
		this.pageTextId = pageTextId;
	}
	public int[] getPagePopupTitleId() {
		return pagePopupTitleId;
	}
	public void setPagePopupTitleId(int[] pagePopupTitleId) {
		this.pagePopupTitleId = pagePopupTitleId;
	}
	public int[] getPagePopupImageId() {
		return pagePopupImageId;
	}
	public void setPagePopupImageId(int[] pagePopupImageId) {
		this.pagePopupImageId = pagePopupImageId;
	}
	public int[] getPagePopupTextId() {
		return pagePopupTextId;
	}
	public void setPagePopupTextId(int[] pagePopupTextId) {
		this.pagePopupTextId = pagePopupTextId;
	}
	public int[] getPagePopupMultimediaId() {
		return pagePopupMultimediaId;
	}
	public void setPagePopupMultimediaId(int[] pagePopupMultimediaId) {
		this.pagePopupMultimediaId = pagePopupMultimediaId;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public int getHasPopup() {
		return hasPopup;
	}
	public void setHasPopup(int hasPopup) {
		this.hasPopup = hasPopup;
	}
	

}
