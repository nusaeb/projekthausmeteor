package projekthausmeteor.data.model;

public class Question {
	
	private long id;
	private String questionType;
	private String question;
	private String possibleAnswers;
	private String correctAnswers;
	private String comment;
	private int questionForAreaId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getPossibleAnswers() {
		return possibleAnswers;
	}
	public void setPossibleAnswers(String possibleAnswers) {
		this.possibleAnswers = possibleAnswers;
	}
	public String getCorrectAnswers() {
		return correctAnswers;
	}
	public void setCorrectAnswers(String correctAnswers) {
		this.correctAnswers = correctAnswers;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getQuestionForAreaId() {
		return questionForAreaId;
	}
	public void setQuestionForAreaId(int questionForAreaId) {
		this.questionForAreaId = questionForAreaId;
	}

}
