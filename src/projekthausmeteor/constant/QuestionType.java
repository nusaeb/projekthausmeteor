
package projekthausmeteor.constant;

public enum QuestionType {

	MULTIPLE_ANSWER,

	SINGLE_ANSWER,

	TRUE_FALSE,
	
	FILL_IN_THE_BLANKS;
}
